import tensorflow as tf
import unreal_engine as ue
from TFPluginAPI import TFPluginAPI
import glob
import io
import math
import os
import numpy as np
import pandas as pd
from sklearn import metrics
import tensorflow as tf
from tensorflow.python.data import Dataset

actionMapping = {"shoot" : 0, "crouch" : 1, "move" : 2 }
modelPath = "NNAImodel.ckpt"
hidden_units = [10, 10, 10]
learning_rate=0.001

class NeuralNetworkAI(TFPluginAPI):

	def onSetup(self):
		self.sess = tf.InteractiveSession()
		self.graph = tf.get_default_graph()

	def onJsonInput(self, jsonInput):

	    my_optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)
	    my_optimizer = tf.contrib.estimator.clip_gradients_by_norm(my_optimizer, 5.0)

		dnn_regressor = tf.estimator.DNNRegressor(
 	      feature_columns=construct_feature_columns(),
 	      hidden_units=hidden_units,
 	      optimizer=my_optimizer,
 		  model_dir=os.getcwd()
 	    )

		training_examples, test_targets = getTestData(jsonInput)
		predict_test_input_fn = lambda: my_input_fn(training_examples,
	                                                    test_targets["action"],
	                                                    num_epochs=1,
	                                                    shuffle=False)
		test_predictions = dnn_regressor.predict(input_fn=predict_testing_input_fn)
		test_predictions = np.array([item['predictions'][0] for item in test_predictions])


		result = { "test_predictions" : test_predictions}
		return result

	def preprocess_features(FPSDataset):
  		selectedFeatures = FPSDataset[
		    ["enemySeen",
		     "enemyHear",
		     "health",
		     "xCoordinate",
		     "yCoordinate",
		     "zCoordinate"]]
  	 	return selected_features

	def preprocess_targets(FPSDataset):
	  output_targets = pd.DataFrame()
	  output_targets["action"] = (
	    FPSDataset["action"])
	  return output_targets

	def construct_feature_columns():

	    enemySeen = tf.feature_column.numeric_column("enemySeen")
	    enemyHear = tf.feature_column.numeric_column("enemyHear")
	    xCoordinate = tf.feature_column.numeric_column("xCoordinate")
	    yCoordinate = tf.feature_column.numeric_column("yCoordinate")
	    zCoordinate = tf.feature_column.numeric_column("zCoordinate")
	    health = tf.feature_column.numeric_column("health")

	    feature_columns = set([enemySeen, enemyHear, xCoordinate, yCoordinate, zCoordinate, health])

    	return feature_columns

	def modifyInputFrame(FPSDataset):
		enemySeen = pd.Series(FPSDataset[0][1:])
		enemyHear = pd.Series(FPSDataset[1][1:])
		health = pd.Series(FPSDataset[2][1:])
		xCoordinate = pd.Series(FPSDataset[3][1:])
		yCoordinate = pd.Series(FPSDataset[4][1:])
		xCoordinate = pd.Series(FPSDataset[5][1:])
		action = pd.Series(FPSDataset[6][1:])

		FPSDataset = pd.DataFrame({ "enemySeen" : enemySeen, "enemyHear" : enemyHear, "health" : health, "xCoordinate" : xCoordinate
				"yCoordinate" : yCoordinate, "zCoordinate" : zCoordinate, "action" : action})

		for i in FPSDataset:
    		FPSDataset[i] = FPSDataset[i].astype(float)

		return FPSDataset

	def my_input_fn(features, targets, batch_size=1, shuffle=False, num_epochs=None):

	    features = {key:np.array(value) for key,value in dict(features).items()}
	    print(targets).
	    ds = Dataset.from_tensor_slices((features,targets))
	    ds = ds.batch(batch_size).repeat(num_epochs)
	    if shuffle:
	      ds = ds.shuffle(1)
	    features, labels = ds.make_one_shot_iterator().get_next()
	    return features, labels

	def trainWithInput(self, inputString):

		FPSDataset = pd.read_csv(
  			io.open("FPSDataset.csv", "r"), sep=",", header=None)

		FPSDataset = modifyInputFrame(FPSDataset)

		training_examples = preprocess_features(FPSDataset.head(6))
		training_targets = preprocess_targets(FPSDataset.head(6))
		validation_examples = preprocess_features(FPSDataset.tail(3))
		validation_targets = preprocess_targets(FPSDataset.tail(3))

		dnn_regressor = train_nn_regression_model(
		    learning_rate=learning_rate,
		    steps=1,
		    batch_size=1,
		    hidden_units=hidden_units,
		    training_examples=training_examples,
		    training_targets=training_targets,
		    validation_examples=validation_examples,
		    validation_targets=validation_targets)


	def onBeginTraining(learning_rate, steps, batch_size, hidden_units, training_examples, training_targets, validation_examples, validation_targets):
		periods = 1
		steps_per_period = steps / periods
		sess = tf.Session()
		saver = tf.train.Saver()

	    my_optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)
	    my_optimizer = tf.contrib.estimator.clip_gradients_by_norm(my_optimizer, 5.0)
	    dnn_regressor = tf.estimator.DNNRegressor(
	      feature_columns=construct_feature_columns(),
	      hidden_units=hidden_units,
	      optimizer=my_optimizer,
		  model_dir=os.getcwd()
	    )

	    training_input_fn = lambda: my_input_fn(training_examples,
	                                          training_targets["action"],
	                                          batch_size=batch_size)
	    predict_training_input_fn = lambda: my_input_fn(training_examples,
	                                                  training_targets["action"],
	                                                  num_epochs=1,
	                                                  shuffle=False)
	    predict_validation_input_fn = lambda: my_input_fn(validation_examples,
	                                                    validation_targets["action"],
	                                                    num_epochs=1,
	                                                    shuffle=False)

	  	ue.log("Training model Start")
	  	training_rmse = []
	  	validation_rmse = []
	  	for period in range (0, periods):
	    	dnn_regressor.train(
	        	input_fn=training_input_fn,
	        	steps=steps_per_period
	    	)
	    	training_predictions = dnn_regressor.predict(input_fn=predict_training_input_fn)
	    	training_predictions = np.array([item['predictions'][0] for item in training_predictions])

	    	validation_predictions = dnn_regressor.predict(input_fn=predict_validation_input_fn)
	    	validation_predictions = np.array([item['predictions'][0] for item in validation_predictions])
	    	ue.log(validation_predictions)

	    	training_root_mean_squared_error = math.sqrt(
	        	metrics.mean_squared_error(training_predictions, training_targets))
	    	validation_root_mean_squared_error = math.sqrt(
	        	metrics.mean_squared_error(validation_predictions, validation_targets))
	    	training_rmse.append(training_root_mean_squared_error)
	    	validation_rmse.append(validation_root_mean_squared_error)

	  ue.log("Model training finished.")
	  save_path = saver.save(sess, model_path)

	  ue.log("Final RMSE (on validation data): %0.2f" % validation_root_mean_squared_error)

	  return dnn_regressor

	def onStopTraining(self):
		pass

def getApi():
	return NeuralNetworkAI.getInstance()
