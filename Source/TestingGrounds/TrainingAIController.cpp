﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "TrainingAIController.h"
#include "AI/Navigation/NavigationPath.h"
#include "Runtime/AIModule/Classes/AITypes.h"
#include "Runtime/AIModule/Classes/AIController.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavigationSystem.h"
#include "EngineUtils.h"
#include "Runtime/Engine/Classes/Engine/StaticMeshActor.h"
#include "TimerManager.h"
#include "Runtime/Core/Public/Misc/FileHelper.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Components/SphereComponent.h"
#include "Runtime/Core/Public/HAL/PlatformFilemanager.h"
#include "Runtime/Engine/Public/EngineGlobals.h"
#include "Engine/Engine.h" 
#include "Runtime/Core/Public/Misc/Paths.h"
#include "Runtime/Core/Public/Containers/UnrealString.h"

FTimerHandle UnusedHandle1;
FTimerHandle UnusedHandle2;
FTimerHandle UnusedHandle3;
FTimerHandle UnusedHandle4;

FTimerHandle UnusedHandle11;
FTimerHandle UnusedHandle21;
FTimerHandle UnusedHandle31;
FTimerHandle UnusedHandle41;

FVector destination;

FVector destination4 = FVector(-3960, -2430, 230);
FVector destination3 = FVector(-1620, 1330, 230);
FVector destination2 = FVector(2650, -620, 230);


FVector waypoint;


float movementRadius = 200;
FVector nextWaypoint;
float reachNextWaypoint = false;
float trainingTweakRange = 200.0f;


USphereComponent * PickupMesh;

ATrainingAIController::ATrainingAIController(const FObjectInitializer& objectInitializer)
{
	//UE_LOG(LogTemp, Warning, TEXT("instance created"));
	static ConstructorHelpers::FObjectFinder<USoundWave> prebattle1Obj(TEXT("/Game/Sounds/prebattle1"));
	prebattle1Wave = prebattle1Obj.Object;
	prebattle1Base = Cast<USoundBase>(prebattle1Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave> prebattle2Obj(TEXT("/Game/Sounds/prebattle2"));
	prebattle2Wave = prebattle2Obj.Object;
	prebattle2Base = Cast<USoundBase>(prebattle2Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave> mandown1Obj(TEXT("/Game/Sounds/mandown1"));
	mandown1Wave = mandown1Obj.Object;
	mandown1Base = Cast<USoundBase>(mandown1Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave> mandown2Obj(TEXT("/Game/Sounds/mandown2"));
	mandown2Wave = mandown2Obj.Object;
	mandown2Base = Cast<USoundBase>(mandown2Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave> mandown3Obj(TEXT("/Game/Sounds/mandown3"));
	mandown3Wave = mandown3Obj.Object;
	mandown3Base = Cast<USoundBase>(mandown3Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave> mandown4Obj(TEXT("/Game/Sounds/mandown4"));
	mandown4Wave = mandown4Obj.Object;
	mandown4Base = Cast<USoundBase>(mandown4Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave> moving1Obj(TEXT("/Game/Sounds/moving1"));
	moving1Wave = moving1Obj.Object;
	moving1Base = Cast<USoundBase>(moving1Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave> moving2Obj(TEXT("/Game/Sounds/moving2"));
	moving2Wave = moving2Obj.Object;
	moving2Base = Cast<USoundBase>(moving1Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave> moving3Obj(TEXT("/Game/Sounds/moving3"));
	moving3Wave = moving3Obj.Object;
	moving3Base = Cast<USoundBase>(moving3Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave> moving4Obj(TEXT("/Game/Sounds/moving4"));
	moving4Wave = moving4Obj.Object;
	moving4Base = Cast<USoundBase>(moving4Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave> moving5Obj(TEXT("/Game/Sounds/moving5"));
	moving5Wave = moving5Obj.Object;
	moving5Base = Cast<USoundBase>(moving5Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave> reloading1Obj(TEXT("/Game/Sounds/reloading1"));
	reloading1Wave = reloading1Obj.Object;
	reloading1Base = Cast<USoundBase>(reloading1Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave> reloading2Obj(TEXT("/Game/Sounds/reloading2"));
	reloading2Wave = reloading2Obj.Object;
	reloading2Base = Cast<USoundBase>(reloading2Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave> reloading3Obj(TEXT("/Game/Sounds/reloading3"));
	reloading3Wave = reloading3Obj.Object;
	reloading3Base = Cast<USoundBase>(reloading3Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave> reloading4Obj(TEXT("/Game/Sounds/reloading4"));
	reloading4Wave = reloading4Obj.Object;
	reloading4Base = Cast<USoundBase>(reloading4Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave> rifleburst1Obj(TEXT("/Game/Sounds/rifleburst1"));
	rifleburst1Wave = rifleburst1Obj.Object;
	rifleburst1Base = Cast<USoundBase>(rifleburst1Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave> rifleburst2Obj(TEXT("/Game/Sounds/rifleburst2"));
	rifleburst2Wave = rifleburst2Obj.Object;
	rifleburst2Base = Cast<USoundBase>(rifleburst2Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave> shoot1Obj(TEXT("/Game/Sounds/shoot1"));
	shoot1Wave = shoot1Obj.Object;
	shoot1Base = Cast<USoundBase>(shoot1Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave> shoot2Obj(TEXT("/Game/Sounds/shoot2"));
	shoot2Wave = shoot2Obj.Object;
	shoot2Base = Cast<USoundBase>(shoot2Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave> shoot3Obj(TEXT("/Game/Sounds/shoot3"));
	shoot3Wave = shoot3Obj.Object;
	shoot3Base = Cast<USoundBase>(shoot3Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave> shoot4Obj(TEXT("/Game/Sounds/shoot4"));
	shoot4Wave = shoot4Obj.Object;
	shoot4Base = Cast<USoundBase>(shoot4Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave> shoot5Obj(TEXT("/Game/Sounds/shoot5"));
	shoot5Wave = shoot5Obj.Object;
	shoot5Base = Cast<USoundBase>(shoot5Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave> shoot6Obj(TEXT("/Game/Sounds/shoot6"));
	shoot6Wave = shoot6Obj.Object;
	shoot6Base = Cast<USoundBase>(shoot6Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave> spotenemy1Obj(TEXT("/Game/Sounds/spotenemy1"));
	spotenemy1Wave = spotenemy1Obj.Object;
	spotenemy1Base = Cast<USoundBase>(spotenemy1Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave> spotenemy2Obj(TEXT("/Game/Sounds/spotenemy2"));
	spotenemy2Wave = spotenemy2Obj.Object;
	spotenemy2Base = Cast<USoundBase>(spotenemy2Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave> spotenemy3Obj(TEXT("/Game/Sounds/spotenemy3"));
	spotenemy3Wave = spotenemy3Obj.Object;
	spotenemy3Base = Cast<USoundBase>(spotenemy3Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave> spotenemy4Obj(TEXT("/Game/Sounds/spotenemy4"));
	spotenemy4Wave = spotenemy4Obj.Object;
	spotenemy4Base = Cast<USoundBase>(spotenemy4Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave> spotenemy5Obj(TEXT("/Game/Sounds/spotenemy5"));
	spotenemy5Wave = spotenemy5Obj.Object;
	spotenemy5Base = Cast<USoundBase>(spotenemy5Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave> spotenemy6Obj(TEXT("/Game/Sounds/spotenemy6"));
	spotenemy6Wave = spotenemy6Obj.Object;
	spotenemy6Base = Cast<USoundBase>(spotenemy6Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave> spotenemy7Obj(TEXT("/Game/Sounds/spotenemy7"));
	spotenemy7Wave = spotenemy7Obj.Object;
	spotenemy7Base = Cast<USoundBase>(spotenemy7Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave>transmission1Obj(TEXT("/Game/Sounds/transmission1"));
	transmission1Wave = transmission1Obj.Object;
	transmission1Base = Cast<USoundBase>(transmission1Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave>transmission2Obj(TEXT("/Game/Sounds/transmission2"));
	transmission2Wave = transmission2Obj.Object;
	transmission2Base = Cast<USoundBase>(transmission2Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave>transmission3Obj(TEXT("/Game/Sounds/transmission3"));
	transmission3Wave = transmission3Obj.Object;
	transmission3Base = Cast<USoundBase>(transmission3Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave>transmission4Obj(TEXT("/Game/Sounds/transmission4"));
	transmission4Wave = transmission1Obj.Object;
	transmission4Base = Cast<USoundBase>(transmission4Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave>transmission5Obj(TEXT("/Game/Sounds/transmission5"));
	transmission5Wave = transmission5Obj.Object;
	transmission5Base = Cast<USoundBase>(transmission5Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave>transmission6Obj(TEXT("/Game/Sounds/transmission6"));
	transmission6Wave = transmission6Obj.Object;
	transmission6Base = Cast<USoundBase>(transmission6Wave);
	static ConstructorHelpers::FObjectFinder<USoundWave>transmission7Obj(TEXT("/Game/Sounds/transmission7"));
	transmission7Wave = transmission7Obj.Object;
	transmission7Base = Cast<USoundBase>(transmission7Wave);



	PickupMesh = CreateDefaultSubobject<USphereComponent>(TEXT("PickupMesh"));
	RootComponent = PickupMesh;

	PickupMesh->SetSphereRadius(80);
	PickupMesh->SetHiddenInGame(false);

	PickupMesh->OnComponentBeginOverlap.AddDynamic(this, &ATrainingAIController::OnCollision);
	OnActorBeginOverlap.AddDynamic(this, &ATrainingAIController::OnActorCollision);
	
	PickupMesh->OnComponentHit.AddDynamic(this, &ATrainingAIController::OnComponentHit);
}

void ATrainingAIController::OnCollision(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	//UE_LOG(LogTemp, Warning, TEXT("overLappedActor: %s"), *OverlappedActor->GetName());
	/*
	UE_LOG(LogTemp, Warning, TEXT("otherActor: %s"), *OtherActor->GetName());
	UE_LOG(LogTemp, Warning, TEXT("OverlappedComp: %s"), *OverlappedComp->GetName());
	UE_LOG(LogTemp, Warning, TEXT("OtherComp: %s"), *OtherComp->GetName());
	*/
}

void ATrainingAIController::OnComponentHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hitt)
{
	
	UE_LOG(LogTemp, Warning, TEXT("HitComponent: %s"), *HitComp->GetName());
	UE_LOG(LogTemp, Warning, TEXT("otherActor: %s"), *OtherActor->GetName());
	UE_LOG(LogTemp, Warning, TEXT("OtherComponent: %s"), *OtherComp->GetName());
	
}

void ATrainingAIController::OnActorCollision(AActor* OverlappedActor, AActor* OtherActor)
{
	/*
	UE_LOG(LogTemp, Warning, TEXT("OverlappedActor: %s"), *OverlappedActor->GetName());
	UE_LOG(LogTemp, Warning, TEXT("otherActor: %s"), *OtherActor->GetName());
	*/
}


void ATrainingAIController::BeginPlay()
{
	Super::BeginPlay();
	//OnActorBeginOverlap.AddDynamic(this, &ATrainingAIController::OnCollision);

	reachNextWaypoint = false;

	trainingInCombat = false;
	trainingUnderCover = false;
	//trainingHasTarget = false;
	trainingUnderAttack = false;
	distanceToCurrentTarget = true;

	health = 100;
	isDead = false;

	canTakeAction = true;
	actionDestination = FVector(0, 0, 0);
	SetCurrentTarget();
	canDelay = true;
	canDialogueSpotEnemy = true;

	destination = destination2;
	MoveToDestination();
	GetTeammatesStats();
	previousPos = GetPawn()->GetActorLocation();
	canStartCombatLockDetect = true;


	if (GetPawn()->GetName().Equals("Trainee1"))
	{
		GetWorldTimerManager().SetTimer(
			UnusedHandle1, this, &ATrainingAIController::LockDetector, 0.65, false);
	}
	else if (GetPawn()->GetName().Equals("Trainee2"))
	{
		GetWorldTimerManager().SetTimer(
			UnusedHandle2, this, &ATrainingAIController::LockDetector, 0.65, false);
	}
	else if (GetPawn()->GetName().Equals("Trainee3"))
	{
		GetWorldTimerManager().SetTimer(
			UnusedHandle3, this, &ATrainingAIController::LockDetector, 0.65, false);
	}
	else if (GetPawn()->GetName().Equals("Trainee4"))
	{
		GetWorldTimerManager().SetTimer(
			UnusedHandle4, this, &ATrainingAIController::LockDetector, 0.65, false);
	}
	else
	{

	}

	ReadRecord();
}

void ATrainingAIController::Tester()
{
	UE_LOG(LogTemp, Warning, TEXT("ayyy lmao"));
	GetWorldTimerManager().SetTimer(
		UnusedHandle1, this, &ATrainingAIController::Tester, 2, false);
}

void ATrainingAIController::PlayVoice(int index)
{

}

void ATrainingAIController::PlayDialoguePreBattle()
{
	int chance = FMath::RandRange(0, 200);
	//UE_LOG(LogTemp, Warning, TEXT("%s get a chance: %d"), *GetPawn()->GetName(), chance);
	if (chance <= 2)
	{
		//UE_LOG(LogTemp, Warning, TEXT("said prebattle"));
		int type = FMath::RandRange(0, 4);
		if (type == 0)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), prebattle1Base, 1);
		}
		else if (type == 1)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), prebattle2Base, 1);
		}
		else if (type == 2)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), transmission2Base, 1);
		}
		else if (type == 3)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), transmission3Base, 1);
		}
		else
		{
			UGameplayStatics::PlaySound2D(GetWorld(), transmission7Base, 1);
		}

	}
}


void ATrainingAIController::PlayDialogueSpotEnemy()
{
	int chance = FMath::RandRange(0, 100);
	if (chance <= 30)
	{
		int type = FMath::RandRange(0, 4);
		if (type == 0)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), spotenemy1Base, 1);
		}
		else if (type == 1)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), spotenemy2Base, 1);
		}
		else if (type == 2)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), spotenemy3Base, 1);
		}
		else if (type == 3)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), spotenemy6Base, 1);
		}
		else
		{
			UGameplayStatics::PlaySound2D(GetWorld(), spotenemy7Base, 1);
		}
	}
}

void ATrainingAIController::PlayDialogueLoading()
{
	int chance = FMath::RandRange(0, 100);
	
	if (chance <= 10)
	{
		int type = FMath::RandRange(0, 4);
		if (type == 0)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), moving1Base, 1);
		}
		else if (type == 1)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), moving2Base, 1);
		}
		else if (type == 2)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), moving3Base, 1);
		}
		else if (type == 3)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), moving4Base, 1);
		}
		else
		{
			UGameplayStatics::PlaySound2D(GetWorld(), moving5Base, 1);
		}
	}
}

void ATrainingAIController::PlayDialogueMoving()
{
	int chance = FMath::RandRange(0, 100);
	if (chance <= 8)
	{
		int type = FMath::RandRange(0, 4);
		if (type == 0)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), reloading1Base, 1);
		}
		else if (type == 1)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), reloading2Base, 1);
		}
		else if (type == 2)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), reloading3Base, 1);
		}
		else if (type == 3)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), reloading4Base, 1);
		}
		else
		{
			UGameplayStatics::PlaySound2D(GetWorld(), spotenemy4Base, 1);
		}
	}
}

void ATrainingAIController::PlayDialogueTransmission()
{
	int chance = FMath::RandRange(0, 100);
	if (chance <= 8)
	{
		int type = FMath::RandRange(0, 4);
		if (type == 0)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), transmission1Base, 1);
		}
		else if (type == 1)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), transmission3Base, 1);
		}
		else if (type == 2)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), transmission4Base, 1);
		}
		else if (type == 3)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), transmission5Base, 1);
		}
		else
		{
			UGameplayStatics::PlaySound2D(GetWorld(), shoot1Base, 1);
		}
	}
}

void ATrainingAIController::PlayDialogueMandown()
{
	int chance = FMath::RandRange(0, 100);
	if (chance <= 50)
	{
		int type = FMath::RandRange(0, 4);
		if (type == 0)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), mandown1Base, 1);
		}
		else if (type == 1)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), mandown2Base, 1);
		}
		else if (type == 2)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), mandown3Base, 1);
		}
		else if (type == 3)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), mandown4Base, 1);
		}
		else
		{
			//
		}
	}
}

void ATrainingAIController::PlayDialogueShoot()
{
	int chance = FMath::RandRange(0, 100);
	if (chance <= 8)
	{
		int type = FMath::RandRange(0, 4);
		if (type == 0)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), shoot4Base, 1);
		}
		else if (type == 1)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), shoot5Base, 1);
		}
		else if (type == 2)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), shoot6Base, 1);
		}
		else if (type == 3)
		{
			UGameplayStatics::PlaySound2D(GetWorld(), shoot3Base, 1);
		}
		else
		{
			UGameplayStatics::PlaySound2D(GetWorld(), shoot2Base, 1);
		}
	}
}

void ATrainingAIController::ResetActionAvailability()
{
	canTakeAction = true;
}

void ATrainingAIController::GetTeammatesStats()
{
	TActorIterator< AActor > ActorItr = TActorIterator< AActor >(GetWorld());
	while (ActorItr)
	{
		if (GetPawn()->GetName().Equals("Trainee1")) 
		{
			if (ActorItr->GetName().Equals("Trainee2"))
			{
				otherTrainee1 = *ActorItr;
			}
			if (ActorItr->GetName().Equals("Trainee3"))
			{
				otherTrainee2 = *ActorItr;
			}
			if (ActorItr->GetName().Equals("Trainee4"))
			{
				otherTrainee3 = *ActorItr;
			}
		}

		if (GetPawn()->GetName().Equals("Trainee2"))
		{
			if (ActorItr->GetName().Equals("Trainee1"))
			{
				otherTrainee1 = *ActorItr;
			}
			if (ActorItr->GetName().Equals("Trainee3"))
			{
				otherTrainee2 = *ActorItr;
			}
			if (ActorItr->GetName().Equals("Trainee4"))
			{
				otherTrainee3 = *ActorItr;
			}
		}

		if (GetPawn()->GetName().Equals("Trainee3"))
		{
			if (ActorItr->GetName().Equals("Trainee1"))
			{
				otherTrainee1 = *ActorItr;
			}
			if (ActorItr->GetName().Equals("Trainee2"))
			{
				otherTrainee2 = *ActorItr;
			}
			if (ActorItr->GetName().Equals("Trainee4"))
			{
				otherTrainee3 = *ActorItr;
			}
		}

		if (GetPawn()->GetName().Equals("Trainee4"))
		{
			if (ActorItr->GetName().Equals("Trainee1"))
			{
				otherTrainee1 = *ActorItr;
			}
			if (ActorItr->GetName().Equals("Trainee2"))
			{
				otherTrainee2 = *ActorItr;
			}
			if (ActorItr->GetName().Equals("Trainee3"))
			{
				otherTrainee3 = *ActorItr;
			}
		}


		++ActorItr;
	}


	if (GetPawn()->GetName().Equals("Trainee1"))
	{
		ATrainingAIController *controller = Cast<ATrainingAIController>(otherTrainee1->GetInstigatorController());
		//UE_LOG(LogTemp, Warning, TEXT("incomabt: %d "), controller->GetHealth());
	}


}

void ATrainingAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (isDead) 
	{
		//do stuff
		if (canDie)
		{
			GetPawn()->SetActorLocation(graveyard);
			PlayDialogueMandown();
			canDie = false;
			WriteRecordsToFile();
			if (GetPawn()->GetName().Equals("Trainee1"))
			{
				manDownNum++;
				if (manDownNum == 4)
				{
					GetWorld()->GetFirstPlayerController()->ConsoleCommand("quit");
				}
			}
			else if (GetPawn()->GetName().Equals("Trainee2"))
			{
				ATrainingAIController *controller = Cast<ATrainingAIController>(otherTrainee1->GetInstigatorController());
				controller->manDownNum++;
				if (controller->manDownNum == 4)
				{
					GetWorld()->GetFirstPlayerController()->ConsoleCommand("quit");
				}
			}
			else if (GetPawn()->GetName().Equals("Trainee3"))
			{
				ATrainingAIController *controller = Cast<ATrainingAIController>(otherTrainee1->GetInstigatorController());
				controller->manDownNum++;
				if (controller->manDownNum == 4)
				{
					GetWorld()->GetFirstPlayerController()->ConsoleCommand("quit");
				}
			}
			else if (GetPawn()->GetName().Equals("Trainee4"))
			{
				ATrainingAIController *controller = Cast<ATrainingAIController>(otherTrainee1->GetInstigatorController());
				controller->manDownNum++;
				if (controller->manDownNum == 4)
				{
					GetWorld()->GetFirstPlayerController()->ConsoleCommand("quit");
				}
			}
		}

		return;
	}
	
	if (!trainingInCombat)
	{
		if (reachNextWaypoint)
		{
			//UE_LOG(LogTemp, Warning, TEXT("reach"));

			if (GetPawn()->GetName().Equals("Trainee1"))
			{
				MoveToDestination();
			}
			else if (GetPawn()->GetName().Equals("Trainee2"))
			{
				MoveToDestination();
			}
			else if (GetPawn()->GetName().Equals("Trainee3"))
			{
				MoveToDestination();
			}
			else if (GetPawn()->GetName().Equals("Trainee4"))
			{
				MoveToDestination();
			}
			else
			{

			}
			reachNextWaypoint = false;
		}
	}

	if (FVector::Dist(GetPawn()->GetActorLocation(), waypoint) <= 100)
	{
		//UE_LOG(LogTemp, Warning, TEXT("made it"));
		reachNextWaypoint = true;
	}

	if (GetPawn()->GetActorLocation().X <= 2650 && GetPawn()->GetActorLocation().X>= -1420)
	{
		destination = destination3;
	}


	if (GetPawn()->GetActorLocation().X <= -1420)
	{
		destination = destination4;
	}

	//distanceToCurrentTarget state distinguishing
	if (FVector::Dist(GetPawn()->GetActorLocation(), trainingCurrentTarget->GetActorLocation()) <= distanceJudger)
	{
		distanceToCurrentTarget = false;
	}
	else
	{
		distanceToCurrentTarget = true;
	}
	
	//engage enemy
	if (FVector::Dist(GetPawn()->GetActorLocation(), trainingCurrentTarget->GetActorLocation()) <= 1400)
	{
		trainingInCombat = true;
		if (canStartCombatLockDetect)
		{
			canStartCombatLockDetect = false;
			CombatLockDetector();
		}
		if (canDialogueSpotEnemy)
		{
			canDialogueSpotEnemy = false;
			PlayDialogueSpotEnemy();
		}
	}
	//combat behavior
	if (trainingInCombat)
	{

		//(LogTemp, Warning, TEXT("%s can take action: %d"), *GetPawn()->GetName(), canTakeAction);
		if (canTakeAction)
		{
			//UE_LOG(LogTemp, Warning, TEXT("%s take an action from Tick"), *GetPawn()->GetName());
			TakeAction();
		}
		//UE_LOG(LogTemp, Warning, TEXT("dis: %f"), FVector::Dist(GetPawn()->GetActorLocation(), actionDestination));
	}

	if (FVector::Dist(GetPawn()->GetActorLocation(), actionDestination) <= 50)
	{
		canTakeAction = true;
		//UE_LOG(LogTemp, Warning, TEXT("%s reached action destination"), *GetPawn()->GetName());
	}


}

void ATrainingAIController::WaitToResetActionAvailability()
{
	canTakeAction = true;
	canDelay = true;
}


void ATrainingAIController::ResetTotalDamageDealtPeriod()
{
	totalDamageDealt = 0;
}

void ATrainingAIController::TakeAction()
{
	//UE_LOG(LogTemp, Warning, TEXT("%s takes action"), *GetPawn()->GetName());
	if (FVector::Dist(GetPawn()->GetActorLocation(), trainingCurrentTarget->GetActorLocation()) >= chaseDistance)
	{
		TrainingChase();
		return;
	}

	if (FVector::Dist(GetPawn()->GetActorLocation(), trainingCurrentTarget->GetActorLocation()) <= walkAwayDistance)
	{
		TrainingWalkAway();
		return;
	}


	if (trainingType == 0) {


		int actionType = FMath::RandRange(0, 5);

		if (lastActionTook == 3)
		{
			int chanceToPeek = FMath::RandRange(0,1);
			if (chanceToPeek == 0)
			{
				trainingUnderCover = true;
				TrainingPeek();
				return;
			}
			else
			{
				if (actionType == 3)
				{

					TakeAction();
					return;
				}

			}

		}

		lastActionTook = actionType;
		if (actionType == 0)
		{
			if (!trainingUnderCover)
			{
				WriteaRecord(0);
				TrainingShoot();
			}
			else
			{
				TakeAction();
				return;
			}
		}
		else if (actionType == 1)
		{
			TrainingMoveBackwardsTowardsTarget();
			WriteaRecord(1);
			if (trainingUnderCover)
			{
				if (trainingUnderCover)
				{
					if (GetPawn()->GetName().Equals("Trainee1"))
					{
						GetWorldTimerManager().SetTimer(UnusedHandle1, this, &ATrainingAIController::ResetUnderCoverState, 1.5f, false);
					}
					else if (GetPawn()->GetName().Equals("Trainee2"))
					{
						GetWorldTimerManager().SetTimer(UnusedHandle2, this, &ATrainingAIController::ResetUnderCoverState, 1.5f, false);
					}
					else if (GetPawn()->GetName().Equals("Trainee3"))
					{
						GetWorldTimerManager().SetTimer(UnusedHandle3, this, &ATrainingAIController::ResetUnderCoverState, 1.5f, false);
					}
					else if (GetPawn()->GetName().Equals("Trainee4"))
					{
						GetWorldTimerManager().SetTimer(UnusedHandle4, this, &ATrainingAIController::ResetUnderCoverState, 1.5f, false);
					}
				}
			}
		}
		else if (actionType == 2)
		{
			TrainingFlank();
			WriteaRecord(2);
			if (trainingUnderCover)
			{
				if (GetPawn()->GetName().Equals("Trainee1"))
				{
					GetWorldTimerManager().SetTimer(UnusedHandle1, this, &ATrainingAIController::ResetUnderCoverState, 1.5f, false);
				}
				else if (GetPawn()->GetName().Equals("Trainee2"))
				{
					GetWorldTimerManager().SetTimer(UnusedHandle2, this, &ATrainingAIController::ResetUnderCoverState, 1.5f, false);
				}
				else if (GetPawn()->GetName().Equals("Trainee3"))
				{
					GetWorldTimerManager().SetTimer(UnusedHandle3, this, &ATrainingAIController::ResetUnderCoverState, 1.5f, false);
				}
				else if (GetPawn()->GetName().Equals("Trainee4"))
				{
					GetWorldTimerManager().SetTimer(UnusedHandle4, this, &ATrainingAIController::ResetUnderCoverState, 1.5f, false);
				}
			}
		}
		else if (actionType == 3)
		{
			TrainingMoveToClosestCover();
			WriteaRecord(3);
		}
		else if (actionType == 4)
		{
			TrainingMoveForwardTowardsTarget();
			WriteaRecord(4);
			if (trainingUnderCover)
			{
				if (GetPawn()->GetName().Equals("Trainee1"))
				{
					GetWorldTimerManager().SetTimer(UnusedHandle1, this, &ATrainingAIController::ResetUnderCoverState, 1.5f, false);
				}
				else if (GetPawn()->GetName().Equals("Trainee2"))
				{
					GetWorldTimerManager().SetTimer(UnusedHandle2, this, &ATrainingAIController::ResetUnderCoverState, 1.5f, false);
				}
				else if (GetPawn()->GetName().Equals("Trainee3"))
				{
					GetWorldTimerManager().SetTimer(UnusedHandle3, this, &ATrainingAIController::ResetUnderCoverState, 1.5f, false);
				}
				else if (GetPawn()->GetName().Equals("Trainee4"))
				{
					GetWorldTimerManager().SetTimer(UnusedHandle4, this, &ATrainingAIController::ResetUnderCoverState, 1.5f, false);
				}
			}

		}
		else if (actionType == 5)
		{
			if (trainingUnderCover)
			{
				TrainingPeek();
			}
			else
			{
				TakeAction();
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("???"));
		}
	}
	else
	{
		// do read & write
	}

	canTakeAction = false;
	
}


void ATrainingAIController::InflictDamage()
{
	APlayerController* PlayerController = Cast<APlayerController>(GetPawn()->GetInstigatorController());
	if (PlayerController != nullptr)
	{
		// Perform a trace @See LineTraceSingle  
		FHitResult TraceResult(ForceInit);
		//TraceHitForward(PlayerController, TraceResult);

		// If the trace return an actor, inflict some damage to that actor  
		AActor* ImpactActor = TraceResult.GetActor();  //fResult(LineTraceSingleByChannelのFHitResultを使用してもいい)
		if ((ImpactActor != nullptr) && (ImpactActor != this))
		{
			// Create a damage event  
			TSubclassOf<UDamageType> const ValidDamageTypeClass = TSubclassOf<UDamageType>(UDamageType::StaticClass());
			FDamageEvent DamageEvent(ValidDamageTypeClass);

			const float DamageAmount = 25.0f; //ダメージ量  
			ImpactActor->TakeDamage(DamageAmount, DamageEvent, PlayerController, this);
			UE_LOG(LogTemp, Warning, TEXT("Receive damage!"));
		}
	}
}

void ATrainingAIController::TrainingShoot()
{
	UE_LOG(LogTemp, Warning, TEXT("%s starts Shoot"), *GetPawn()->GetName());
	FVector direction = trainingCurrentTarget->GetActorLocation() - GetPawn()->GetActorLocation();

	FRotator Rot = FRotationMatrix::MakeFromX(direction).Rotator();
	GetPawn()->SetActorRotation(Rot);
	int type = FMath::RandRange(0, 1);
	if (type == 0)
	{
		UGameplayStatics::PlaySound2D(GetWorld(), rifleburst1Base, 0.4);
	}
	else if (type == 1)
	{
		UGameplayStatics::PlaySound2D(GetWorld(), rifleburst2Base, 0.4);
	}

	if (GetPawn()->GetName().Equals("Trainee1"))
	{
		//UE_LOG(LogTemp, Warning, TEXT("Shoot1!"));
		GetWorldTimerManager().SetTimer(UnusedHandle1, this, &ATrainingAIController::ResetActionAvailability, 2, false);
		totalDamageDealt += 10;
	}
	else if (GetPawn()->GetName().Equals("Trainee2"))
	{
		//UE_LOG(LogTemp, Warning, TEXT("Shoot2!"));
		GetWorldTimerManager().SetTimer(UnusedHandle2, this, &ATrainingAIController::ResetActionAvailability, 2, false);
		ATrainingAIController *controller = Cast<ATrainingAIController>(otherTrainee1->GetInstigatorController());
		controller->totalDamageDealt += 10;
	}

	else if (GetPawn()->GetName().Equals("Trainee3"))
	{
		//UE_LOG(LogTemp, Warning, TEXT("Shoot3!"));
		GetWorldTimerManager().SetTimer(UnusedHandle3, this, &ATrainingAIController::ResetActionAvailability, 2, false);
		ATrainingAIController *controller = Cast<ATrainingAIController>(otherTrainee1->GetInstigatorController());
		controller->totalDamageDealt += 10;
	}
	else if (GetPawn()->GetName().Equals("Trainee4"))
	{
		//UE_LOG(LogTemp, Warning, TEXT("Shoot4!"));
		GetWorldTimerManager().SetTimer(UnusedHandle4, this, &ATrainingAIController::ResetActionAvailability, 2, false);
		ATrainingAIController *controller = Cast<ATrainingAIController>(otherTrainee1->GetInstigatorController());
		controller->totalDamageDealt += 10;
	}
	else
	{

	}
}

void ATrainingAIController::TrainingChase()
{
	lastActionTook = 7;
	WriteaRecord(7);
	UE_LOG(LogTemp, Warning, TEXT("%s starts Chase"), *GetPawn()->GetName());
	canTakeAction = false;
	FVector pointer = trainingCurrentTarget->GetActorLocation() - GetPawn()->GetActorLocation();

	FVector newPos = GetPawn()->GetActorLocation() + pointer * 5 / 7;
	int xTweak = FMath::RandRange(-trainingTweakRange/2, trainingTweakRange/2);
	int yTweak = FMath::RandRange(-trainingTweakRange/2, trainingTweakRange/2);
	newPos = FVector(newPos.X + xTweak, newPos.Y + yTweak, newPos.Z);
	MoveToPosition(newPos, 50);
	UpdateActionDestination(newPos);
	PlayDialogueMoving();

}


void ATrainingAIController::SetUnderCoverState() 
{
	trainingUnderCover = true;
}
void ATrainingAIController::ResetUnderCoverState()
{
	trainingUnderCover = false;
}
void ATrainingAIController::ResetUnderAttackState()
{
	trainingUnderAttack = false;
}

void ATrainingAIController::ReceiveDamage()
{
	if (health > 0)
	{
		//UE_LOG(LogTemp, Warning, TEXT("%s took damage, remain health: %d"), *GetPawn()->GetName(), health);
		health -= receivedDamage;
	}
	else
	{
		isDead = true;
	}
}

void ATrainingAIController::TrainingShootPeek()
{
	UE_LOG(LogTemp, Warning, TEXT("%s starts ShootPeek"), *GetPawn()->GetName());
	FVector direction = trainingCurrentTarget->GetActorLocation() - GetPawn()->GetActorLocation();

	FRotator Rot = FRotationMatrix::MakeFromX(direction).Rotator();
	GetPawn()->SetActorRotation(Rot);

	if (GetPawn()->GetName().Equals("Trainee1"))
	{
		//UE_LOG(LogTemp, Warning, TEXT("Shoot1!"));
		totalDamageDealt += 10;
	}
	else
	{
		ATrainingAIController *controller = Cast<ATrainingAIController>(otherTrainee1->GetInstigatorController());
		controller->totalDamageDealt += 10;
	}
	int type = FMath::RandRange(0, 1);
	if (type == 0)
	{
		UGameplayStatics::PlaySound2D(GetWorld(), rifleburst1Base, 0.4);
	}
	else if (type == 1)
	{
		UGameplayStatics::PlaySound2D(GetWorld(), rifleburst2Base, 0.4);
	}

}


void  ATrainingAIController::ReadRecord()
{
	projectDir = FPaths::GameDir();
	projectDir += "Content/Records.txt";
	FString FileData = "";


	if (GetPawn()->GetName().Equals("Trainee1"))
	{
		if (!FPlatformFileManager::Get().GetPlatformFile().FileExists(*projectDir))
		{
			UE_LOG(LogTemp, Warning, TEXT("Didnt find the records"));
			return;

		}
		UE_LOG(LogTemp, Warning, TEXT("Found the records"));
		FFileHelper::LoadFileToString(FileData, *projectDir);

		newRecords += FileData;

		int32 lineCount = FileData.ParseIntoArray(records, TEXT("\n"), true);
		UE_LOG(LogTemp, Warning, TEXT("records num: %d"), lineCount);
	}


	//for (int i = 0; i < records.Num(); i++)
	for (FString str : records)
	{
		if (str.Len() < 12)
		{
			OneMenAlive.Add(str);
		}

		if (str.Len() >= 12 && str.Len() <= 13)
		{
			TwoMenAlive.Add(str);
		}

		if (str.Len() > 13 && str.Len() <= 17)
		{
			ThreeMenAlive.Add(str);
		}

		if (str.Len() >= 20)
		{
			FourMenAlive.Add(str);
		}
	}

}


int ATrainingAIController::GetOptimalAction(TArray<FString> arr, FString state)
{
	if (state.Len() <= 9)
	{
		//one men alive
	}

	if (state.Len() >= 12 && state.Len() <= 13)
	{
		//two men alive
	}

	if (state.Len() > 13 && state.Len() <= 17)
	{
		//three men alive
	}

	if (state.Len() >= 20)
	{
		//four men alive
	}



	//removed


	return 0;
}



void ATrainingAIController::WriteaRecord(int actionType)
{
	if (!GetPawn()->GetName().Equals("Trainee1"))
	{
		ATrainingAIController *controller = Cast<ATrainingAIController>(otherTrainee1->GetInstigatorController());
		newRecords = controller->newRecords;
	}

	newRecords+= FString::FromInt(GetIncombatState()) + FString::FromInt(GetUnderCoverState()) +
		FString::FromInt(GetUnderAttackState()) + FString::FromInt(GetDistanceToCurrentTargetState());
	ATrainingAIController *controller1 = Cast<ATrainingAIController>(otherTrainee1->GetInstigatorController());
	if (!controller1->GetDeathState())
	{
		newRecords += FString::FromInt(controller1->GetIncombatState()) + FString::FromInt(controller1->GetUnderCoverState()) +
			FString::FromInt(controller1->GetUnderAttackState()) + FString::FromInt(controller1->GetDistanceToCurrentTargetState());
	}
	ATrainingAIController *controller2 = Cast<ATrainingAIController>(otherTrainee2->GetInstigatorController());
	if (!controller2->GetDeathState())
	{
		newRecords += FString::FromInt(controller2->GetIncombatState()) + FString::FromInt(controller2->GetUnderCoverState()) +
			FString::FromInt(controller2->GetUnderAttackState()) + FString::FromInt(controller2->GetDistanceToCurrentTargetState());
	}
	ATrainingAIController *controller3 = Cast<ATrainingAIController>(otherTrainee3->GetInstigatorController());
	if (!controller3->GetDeathState())
	{
		newRecords += FString::FromInt(controller3->GetIncombatState()) + FString::FromInt(controller3->GetUnderCoverState()) +
			FString::FromInt(controller3->GetUnderAttackState()) + FString::FromInt(controller3->GetDistanceToCurrentTargetState());
	}

	newRecords += FString::FromInt(actionType);
	newRecords += "_";
	if (!GetPawn()->GetName().Equals("Trainee1"))
	{
		ATrainingAIController *controller = Cast<ATrainingAIController>(otherTrainee1->GetInstigatorController());
		newRecords += FString::FromInt(controller->totalDamageDealt);
	}
	else
	{
		newRecords += FString::FromInt(totalDamageDealt);
	}

	newRecords += "\n";
	//WriteRecordWithDamage(newRecords);
	
	//UE_LOG(LogTemp, Warning, TEXT("record: %s"), *newRecords);
}


void ATrainingAIController::WriteRecordWithDamage(FString str)
{
	//newRecords
}

void ATrainingAIController::WriteRecordsToFile()
{
	FFileHelper::SaveStringToFile(*newRecords, *projectDir);
}

int ATrainingAIController::GetDeathState()
{
	return isDead;
}

float ATrainingAIController::DeadZoneCheck(FVector pos)

{
	if (-2180 < pos.X && pos.X < 800 && -2770 <  pos.Y && pos.Y < -770)
	{
		return false;
	}
	if (-3360 < pos.X && pos.X < -1930 && 900 <  pos.Y && pos.Y < 1840)
	{
		return false;
	}

	return true;
}

void ATrainingAIController::LockDetector()
{
	if (trainingInCombat)
		return;
	float lockDelay = FMath::FRandRange(0, 0.3);
	if (GetPawn()->GetName().Equals("Trainee1"))
	{
		GetWorldTimerManager().SetTimer(
			UnusedHandle1, this, &ATrainingAIController::LockDetector, lockDelay, false);
	}
	else if (GetPawn()->GetName().Equals("Trainee2"))
	{
		GetWorldTimerManager().SetTimer(
			UnusedHandle2, this, &ATrainingAIController::LockDetector, lockDelay, false);
	}
	else if (GetPawn()->GetName().Equals("Trainee3"))
	{
		GetWorldTimerManager().SetTimer(
			UnusedHandle3, this, &ATrainingAIController::LockDetector, lockDelay, false);
	}
	else if (GetPawn()->GetName().Equals("Trainee4"))
	{
		GetWorldTimerManager().SetTimer(
			UnusedHandle4, this, &ATrainingAIController::LockDetector, lockDelay, false);
	}
	else
	{

	}
	if (FVector::Dist(GetPawn()->GetActorLocation(), previousPos) <= 20) 
	{
		MoveToDestination();
	}
	previousPos = GetPawn()->GetActorLocation();
}


void ATrainingAIController::MoveToCoverLockDetector()
{
	int lockDelay = FMath::RandRange(1, 2);
	if (GetPawn()->GetName().Equals("Trainee1"))
	{
		//UE_LOG(LogTemp, Warning, TEXT("%s schedued CombatLockDetect"), *GetPawn()->GetName());
		GetWorldTimerManager().SetTimer(UnusedHandle11, this, &ATrainingAIController::CombatLockDetector, lockDelay, false);
	}
	else if (GetPawn()->GetName().Equals("Trainee2"))
	{
		//UE_LOG(LogTemp, Warning, TEXT("%s schedued CombatLockDetect"), *GetPawn()->GetName());
		GetWorldTimerManager().SetTimer(UnusedHandle21, this, &ATrainingAIController::CombatLockDetector, lockDelay, false);
	}
	else if (GetPawn()->GetName().Equals("Trainee3"))
	{
		//UE_LOG(LogTemp, Warning, TEXT("%s schedued CombatLockDetect"), *GetPawn()->GetName());
		GetWorldTimerManager().SetTimer(UnusedHandle31, this, &ATrainingAIController::CombatLockDetector, lockDelay, false);
	}
	else if (GetPawn()->GetName().Equals("Trainee4"))
	{
		//UE_LOG(LogTemp, Warning, TEXT("%s schedued CombatLockDetect"), *GetPawn()->GetName());
		GetWorldTimerManager().SetTimer(UnusedHandle41, this, &ATrainingAIController::CombatLockDetector, lockDelay, false);
	}
}

void ATrainingAIController::CombatLockDetector()
{
	//UE_LOG(LogTemp, Warning, TEXT("%s starts CombatLockDetect"), *GetPawn()->GetName());
	if (isDead)
	{
		return;
	}

	int lockDelay = FMath::RandRange(1, 2);
	if (GetPawn()->GetName().Equals("Trainee1"))
	{
		//UE_LOG(LogTemp, Warning, TEXT("%s schedued CombatLockDetect"), *GetPawn()->GetName());
		GetWorldTimerManager().SetTimer(UnusedHandle11, this, &ATrainingAIController::CombatLockDetector, lockDelay, false);
		combatPreviousPos = GetPawn()->GetActorLocation();
	}
	else if (GetPawn()->GetName().Equals("Trainee2"))
	{
		//UE_LOG(LogTemp, Warning, TEXT("%s schedued CombatLockDetect"), *GetPawn()->GetName());
		GetWorldTimerManager().SetTimer(UnusedHandle21, this, &ATrainingAIController::CombatLockDetector, lockDelay, false);
		combatPreviousPos = GetPawn()->GetActorLocation();
	}
	else if (GetPawn()->GetName().Equals("Trainee3"))
	{
		//UE_LOG(LogTemp, Warning, TEXT("%s schedued CombatLockDetect"), *GetPawn()->GetName());
		GetWorldTimerManager().SetTimer(UnusedHandle31, this, &ATrainingAIController::CombatLockDetector, lockDelay, false);
		combatPreviousPos = GetPawn()->GetActorLocation();
	}
	else if (GetPawn()->GetName().Equals("Trainee4"))
	{
		//UE_LOG(LogTemp, Warning, TEXT("%s schedued CombatLockDetect"), *GetPawn()->GetName());
		GetWorldTimerManager().SetTimer(UnusedHandle41, this, &ATrainingAIController::CombatLockDetector, lockDelay, false);
		combatPreviousPos = GetPawn()->GetActorLocation();
	}
	else
	{

	}
	if (!trainingInCombat)
	{
		return;
	}
	if (FVector::Dist(GetPawn()->GetActorLocation(), combatPreviousPos) <= 100)
	{
		{
			UE_LOG(LogTemp, Warning, TEXT("%s: CombatLock detected, previous move: %d"), *GetPawn()->GetName(), lastActionTook);
			PlayDialogueTransmission();
			TakeAction();
		}
	}

}

void ATrainingAIController::MoveToSecondaryCover()
{

}

float ATrainingAIController::MoveToPosition(FVector destination, float proximity)
{

	UNavigationPath* path = UNavigationSystem::FindPathToLocationSynchronously(Cast<UObject>(GetWorld()), GetPawn()->GetActorLocation(), destination);

	//UE_LOG(LogTemp, Warning, TEXT("l: %f"), path->GetPathLength());

	if (path)
	{

		//UE_LOG(LogTemp, Warning, TEXT("Path Created"));
		FAIMoveRequest req;

		req.SetAcceptanceRadius(proximity);
		req.SetUsePathfinding(true);

		AAIController* ai = Cast<AAIController>(this);

		if (ai)
		{
			ai->RequestMove(req, path->GetPath());
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Couldnt find a path, the destination might not be covered by navmesh"));		//Left for further changes ¯\_(ツ)_/¯
		return false;
	}



	
	return true;
}

void ATrainingAIController::MoveToDestination()
{
	/*
	if (!destination)
	{
		UE_LOG(LogTemp, Warning, TEXT("no destination"));
		return;
	}
	*/

	//UE_LOG(LogTemp, Warning, TEXT("%s moves to destination"), *GetPawn()->GetName());
	if (FVector::Dist(GetPawn()->GetActorLocation(), destination) <= 700) 
	{
		MoveToPosition(destination, 0);
		PlayDialoguePreBattle();
	}
	else
	{
		FVector direction = destination - GetPawn()->GetActorLocation(); 
		FVector unitDireaction = direction / FVector::Dist(destination, GetPawn()->GetActorLocation());
		float distance = FMath::RandRange(800, 1000);
		float xTweak = FMath::RandRange(-500, 500);
		float yTweak = FMath::RandRange(-500, 500);

		FVector newPos = GetPawn()->GetActorLocation() + unitDireaction * distance;
		newPos = FVector(newPos.X + xTweak, newPos.Y + yTweak, newPos.Z);


		if (!DeadZoneCheck(destination))
		{
			MoveToDestination();
			PlayDialoguePreBattle();
			return;
		}


		waypoint = newPos;
		if (MoveToPosition(newPos, 0))
		{
			PlayDialoguePreBattle();
		}
		else 
		{
			MoveToDestination();
			PlayDialoguePreBattle();
		}
	}

}

void ATrainingAIController::TrainingWalkAway()
{
	canTakeAction = false;
	float xTweak = FMath::FRandRange(600, 1200);
	float np = FMath::RandRange(0, 1);
	if (np == 0)
	{
		xTweak = -xTweak;
	}
	float yTweak = FMath::FRandRange(600, 1000);
	np = FMath::RandRange(0, 1);
	if (np == 0)
	{
		yTweak = -yTweak;
	}

	FVector newPos = FVector(trainingCurrentTarget->GetActorLocation().X + xTweak, 
		trainingCurrentTarget->GetActorLocation().Y + yTweak,
		trainingCurrentTarget->GetActorLocation().Z
		);
	MoveToPosition(newPos, 50);
	PlayDialogueMoving();
	UpdateActionDestination(newPos);
}

void ATrainingAIController::SetCurrentTarget() 
{
	TActorIterator< AActor > ActorItr = TActorIterator< AActor >(GetWorld());
	while (ActorItr)
	{
		//UE_LOG(LogTemp, Warning, TEXT("name: %s"), *ActorItr->GetName());
		if (ActorItr->GetName().Equals("Guardian"))
			//if (ActorItr->GetName().Equals("Guard"))
		{
			trainingCurrentTarget = *ActorItr;
			//trainingCurrentTarget->GetInstigatorController().
		}

		++ActorItr;
	}
}

void ATrainingAIController::TrainingPeek() 
{
	UE_LOG(LogTemp, Warning, TEXT("%s starts peek"), *GetPawn()->GetName());
	
	if (!trainingUnderCover)
	{
		//UE_LOG(LogTemp, Warning, TEXT("Tried Peeking, no closest cover, rechoosing"));
		canTakeAction = true;
		TakeAction();
		return;
	}

	lastActionTook = 5;
	
	TActorIterator< AStaticMeshActor > WallItr = TActorIterator< AStaticMeshActor >(GetWorld());
	int minDistance = 9999;
	while (WallItr)
	{
		if (WallItr->GetName().Contains("Wall"))
		{
			if (FVector::Dist(WallItr->GetActorLocation(), GetPawn()->GetActorLocation()) < minDistance)
			{
				minDistance = FVector::Dist(WallItr->GetActorLocation(), GetPawn()->GetActorLocation());
				trainingClosestCover = *WallItr;
			}
		}
		++WallItr;
	}


	float ratio = trainingClosestCover->GetActorRelativeScale3D().X / 2;
	FVector unitDirection = (trainingCurrentTarget->GetActorLocation() - GetPawn()->GetActorLocation() )/ 
		FVector::Dist(trainingCurrentTarget->GetActorLocation() , GetPawn()->GetActorLocation());
	FVector verticalUnitDirection;
	int side = FMath::RandRange(0, 1);
	if (side == 0)
	{
		//UE_LOG(LogTemp, Warning, TEXT("left peek"));
		verticalUnitDirection = FVector(-unitDirection.Y, unitDirection.X, unitDirection.Z);
		//UE_LOG(LogTemp, Warning, TEXT("verticalUnitDirection: %f, %f, %f"), verticalUnitDirection.X, verticalUnitDirection.Y, verticalUnitDirection.Z);
	}
	else
	{
		//UE_LOG(LogTemp, Warning, TEXT("right peek"));
		verticalUnitDirection = FVector(unitDirection.Y, -unitDirection.X, unitDirection.Z);
	}
	peekOldPos = GetPawn()->GetTargetLocation();
	FVector newPos = verticalUnitDirection * ratio * 125  + GetPawn()->GetActorLocation();
	//UE_LOG(LogTemp, Warning, TEXT("newpos: %f, %f, %f"), newPos.X, newPos.Y, newPos.Z);
	isPeeking = true;

	//UE_LOG(LogTemp, Warning, TEXT("%s move out to attack"), *GetPawn()->GetName());
	MoveToPosition(newPos, 0);
	if (GetPawn()->GetName().Equals("Trainee1"))
	{
		GetWorldTimerManager().SetTimer(
			UnusedHandle1, this, &ATrainingAIController::TrainingShootPeek, 1.1, false);
		GetWorldTimerManager().SetTimer(
			UnusedHandle1, this, &ATrainingAIController::TrainingPeekBackToOldPos, 2, false);
	}
	else if (GetPawn()->GetName().Equals("Trainee2"))
	{
		GetWorldTimerManager().SetTimer(
			UnusedHandle2, this, &ATrainingAIController::TrainingShootPeek, 1.1, false);
		GetWorldTimerManager().SetTimer(
			UnusedHandle1, this, &ATrainingAIController::TrainingPeekBackToOldPos, 2, false);
	}
	else if (GetPawn()->GetName().Equals("Trainee3"))
	{
		GetWorldTimerManager().SetTimer(
			UnusedHandle3, this, &ATrainingAIController::TrainingShootPeek, 1.1, false);
		GetWorldTimerManager().SetTimer(
			UnusedHandle1, this, &ATrainingAIController::TrainingPeekBackToOldPos, 2, false);
	}
	else if (GetPawn()->GetName().Equals("Trainee4"))
	{
		GetWorldTimerManager().SetTimer(
			UnusedHandle4, this, &ATrainingAIController::TrainingShootPeek, 1.1, false);
		GetWorldTimerManager().SetTimer(
			UnusedHandle1, this, &ATrainingAIController::TrainingPeekBackToOldPos, 2, false);
	}
	else
	{

	}
}

void ATrainingAIController::TrainingPeekBackToOldPos()
{
	//UE_LOG(LogTemp, Warning, TEXT("%s starts peekBackToOldPos"), *GetPawn()->GetName());
	isPeeking = false;
	MoveToPosition(peekOldPos, 0);
	GetWorldTimerManager().SetTimer(
		UnusedHandle1, this, &ATrainingAIController::ResetActionAvailability, 3, false);
}

void ATrainingAIController::TrainingMoveToClosestCover()
{
	UE_LOG(LogTemp, Warning, TEXT("%s starts MoveToClosestCover"), *GetPawn()->GetName());
	TActorIterator< AStaticMeshActor > WallItr = TActorIterator< AStaticMeshActor >(GetWorld());
	float minDistance = 99999.0f;	//Magic number because I'm lazy

	int count = 0;
	while (WallItr)
	{
		if (WallItr->GetName().Contains("Wall"))
		{
			if (FVector::Dist(WallItr->GetActorLocation(), GetPawn()->GetActorLocation()) < minDistance)
			{
				minDistance = FVector::Dist(WallItr->GetActorLocation(), GetPawn()->GetActorLocation());
				trainingClosestCover = *WallItr;
			}
			//UE_LOG(LogTemp, Warning, TEXT("find: %s"), *WallItr->GetName());
			++count;
		}
		++WallItr;
	}

	//(LogTemp, Warning, TEXT("count: %d"), count);
	if (trainingClosestCover)
	{
		FVector pointer = trainingCurrentTarget->GetActorLocation() - trainingClosestCover->GetActorLocation();
		FVector unitVector = pointer / (FVector::Dist(trainingCurrentTarget->GetActorLocation(), trainingClosestCover->GetActorLocation()));

		FVector newPos = trainingClosestCover->GetActorLocation() - unitVector * 100;
		/*
		if (closestCover->GetActorScale3D().Z == normalWallHeight)

		{
			MoveToPosition(newPos, 0);
		}
		else if (closestCover->GetActorScale3D().Z == parapetHeight)
		{
			MoveToPosition(newPos, 0);	//under construction: need to attach following action
		}
		*/
		GetWorldTimerManager().SetTimer(UnusedHandle1, this, &ATrainingAIController::SetUnderCoverState, 1.5, false);
		GetWorldTimerManager().SetTimer(UnusedHandle1, this, &ATrainingAIController::PlayDialogueLoading, 3, false);
		UpdateActionDestination(newPos);
		MoveToCoverLockDetector();
		MoveToPosition(newPos, 0);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Could not find closest cover, something went wrong!"));
	}
}

void ATrainingAIController::TrainingFlank()
{
	UE_LOG(LogTemp, Warning, TEXT("%s starts Flank"), *GetPawn()->GetName());
	int rotDegree = 45;	//rotation for flanking route
	int side = FMath::RandRange(0, 1);
	FRotator rot(0, rotDegree, 0);
	FVector pointer = trainingCurrentTarget->GetActorLocation() - GetPawn()->GetActorLocation();
	FVector unitPointer = pointer / FVector::Dist(GetPawn()->GetActorLocation(), trainingCurrentTarget->GetActorLocation());
	pointer = unitPointer * 1000;
	FVector newPos;
	float xTweak;
	float yTweak;

	if (side == 1)
	{
		//UE_LOG(LogTemp, Warning, TEXT("flanking from the right"));
	}
	else
	{
		//UE_LOG(LogTemp, Warning, TEXT("flanking from the left"));
		rot.Yaw = -rot.Yaw;

	}
	if (FVector::Dist(trainingCurrentTarget->GetActorLocation(), GetPawn()->GetActorLocation()) <= trainingTweakRange * 2)
	{
		//UE_LOG(LogTemp, Warning, TEXT("Too close, no need for flanking"));	//Might need to be tweaked
		canTakeAction = true;
		return;
	}
	else
	{
		//pointer = pointer * 1.5;
		newPos = GetPawn()->GetActorLocation() + rot.RotateVector(pointer);	//move a little bit further since AI change its' moving direction
	}

	float canReach = MoveToPosition(newPos, 0);
	if (canReach)
	{
		UpdateActionDestination(newPos);
		GetWorldTimerManager().SetTimer(UnusedHandle1, this, &ATrainingAIController::PlayDialogueMoving, 1.5, false);
	}
	while (!canReach)
	{
		pointer = pointer * 2 / 3;
		//UE_LOG(LogTemp, Warning, TEXT("pointer: %f %f %f"), pointer.X, pointer.Y, pointer.Z);
		newPos = GetPawn()->GetActorLocation() + rot.RotateVector(pointer);
		//UE_LOG(LogTemp, Warning, TEXT("newPos: %f %f %f"), newPos.X, newPos.Y, newPos.Z);
		UpdateActionDestination(newPos);
		canReach = MoveToPosition(newPos, 0);
		GetWorldTimerManager().SetTimer(UnusedHandle1, this, &ATrainingAIController::PlayDialogueMoving, 1.5, false);
		UE_LOG(LogTemp, Warning, TEXT("cant reach"));
	}
	xTweak = FMath::RandRange(-trainingTweakRange / 2, trainingTweakRange / 2);
	yTweak = FMath::RandRange(-trainingTweakRange / 2, trainingTweakRange / 2);
	newPos = FVector(newPos.X + xTweak, newPos.Y + yTweak, newPos.Z);
	//UE_LOG(LogTemp, Warning, TEXT("count: %d"), count);
}

void ATrainingAIController::TrainingMoveForwardTowardsTarget()
{
	UE_LOG(LogTemp, Warning, TEXT("%s starts MoveForward"), *GetPawn()->GetName());

	FVector pointer = trainingCurrentTarget->GetActorLocation() - GetPawn()->GetActorLocation();
	FVector newPos;

	if (FVector::Dist(trainingCurrentTarget->GetActorLocation(), GetPawn()->GetActorLocation()) <= trainingTweakRange * 2)
	{
		newPos = GetPawn()->GetActorLocation() + pointer * 3 / 5;

	}
	else
	{
		newPos = GetPawn()->GetActorLocation() + pointer / 2;	//left for further changes

		float xTweak = FMath::FRandRange(-trainingTweakRange, trainingTweakRange);
		float yTweak = FMath::FRandRange(-trainingTweakRange, trainingTweakRange);
		newPos = FVector(newPos.X + xTweak, newPos.Y + yTweak, newPos.Z);

	}
	UpdateActionDestination(newPos);
	MoveToPosition(newPos, 0);
}





void ATrainingAIController::TrainingMoveBackwardsTowardsTarget()
{
	UE_LOG(LogTemp, Warning, TEXT("%s starts MoveBackwards"), *GetPawn()->GetName());
	FVector pointer = GetPawn()->GetActorLocation() - trainingCurrentTarget->GetActorLocation();
	FVector unitPointer = pointer / FVector::Dist(GetPawn()->GetActorLocation(), trainingCurrentTarget->GetActorLocation());
	float rate = FMath::RandRange(3, 8) / 10;
	FVector newPos = GetPawn()->GetActorLocation() + unitPointer * 500 * rate;	//left for further changes

	int xTweak = FMath::RandRange(-trainingTweakRange, trainingTweakRange);
	int yTweak = FMath::RandRange(-trainingTweakRange, trainingTweakRange);
	newPos = FVector(newPos.X + xTweak, newPos.Y + yTweak, newPos.Z);
	UpdateActionDestination(newPos);
	MoveToPosition(newPos, 0);

	GetWorldTimerManager().SetTimer(UnusedHandle1, this, &ATrainingAIController::PlayDialogueMoving, 1, false);
}


void ATrainingAIController::UpdateActionDestination(FVector des) 
{
	actionDestination = des;
}

int ATrainingAIController::GetHealth()
{
	return health;
}

bool ATrainingAIController::GetIncombatState()
{
	return trainingInCombat;
}

bool ATrainingAIController::GetUnderCoverState()
{
	return trainingUnderCover;
}

bool ATrainingAIController::GetUnderAttackState()
{
	return trainingUnderAttack;
}

bool ATrainingAIController::GetDistanceToCurrentTargetState()
{
	return distanceToCurrentTarget;
}