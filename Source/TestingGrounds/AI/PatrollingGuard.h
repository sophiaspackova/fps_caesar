// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "NPC/PatrolRoute.h"
#include "PatrollingGuard.generated.h"

UCLASS()
class TESTINGGROUNDS_API APatrollingGuard : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APatrollingGuard();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	FTransform original;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float Health;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float FireRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		bool CoolDownBullet;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		int Ammo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float ReloadTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		bool CoolDownReload;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
		UPatrolRoute* Patrol;

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void FireGun();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		bool isDead();

	UFUNCTION(BlueprintCallable)
		void RestartGame(); 

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void Chase();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void Suspicious();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void Patrolling();
};
