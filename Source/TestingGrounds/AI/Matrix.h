#pragma once
#include "CoreMinimal.h"
#include <vector>

class Matrix
{
	public:
		Matrix(int numRows, int numCols, bool isRandom);
	
		//Functions
		Matrix* Transpose();
		float CreateRandomWeight();
		Matrix* Multiply(Matrix* other);
		TArray<float> ToVector();

		//Setters
		void SetVal(int r, int c, float v);

		//Getters
		float GetVal(int r, int c);
		int GetNumRows() { return this->numRows; }
		int GetNumCols() { return this->numCols; }
private:

		TArray<TArray<float> > values;

		int numRows;
		int numCols;

};