#include "Layer.h"

Layer::Layer(int size)
{
	this->size = size;

	for (int i = 0; i < size; i++)
	{
		Neuron* n = new Neuron(0.00);
		this->neurons.push_back(n);
	}
}

void Layer::SetVal(int i, float v)
{
	this->neurons[i]->SetVal(v);
}

Matrix * Layer::GetMatrixRepresentation()
{
	Matrix *m = new Matrix(1, this->neurons.size(), false);
	for (int i = 0; i < this->neurons.size(); i++)
	{
		m->SetVal(1, i, this->neurons[i]->GetVal());
	}

	return m;
}

Matrix * Layer::GetActivatedMatrixRepresentation()
{
	Matrix *m = new Matrix(1, this->neurons.size(), false);
	for (int i = 0; i < this->neurons.size(); i++)
	{
		m->SetVal(1, i, this->neurons[i]->GetActivatedVal());
	}

	return m;
}

Matrix * Layer::GetDerivedMatrixRepresentation()
{
	Matrix *m = new Matrix(1, this->neurons.size(), false);
	for (int i = 0; i < this->neurons.size(); i++)
	{
		m->SetVal(1, i, this->neurons[i]->GetDerivedVal());
	}

	return m;
}