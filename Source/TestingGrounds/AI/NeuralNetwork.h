// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include <stdint.h>
#include "Matrix.h"
#include "Layer.h"
#include <vector>
#include "Player/FirstPersonCharacter.h"
#include "NeuralNetwork.generated.h"

USTRUCT()
struct FState
{

	GENERATED_BODY()
public: 

	FState(int numEnemies)
	{
		EnemyCanSee = TArray<bool>();
		EnemyCanHear = TArray<bool>();
		EnemyPosition = TArray<FVector>();
		for (int i = 0; i < numEnemies; i++)
		{
			EnemyCanSee.Add(false);
			EnemyCanHear.Add(false);
			EnemyPosition.Add(FVector());
		}
		ResultVector = TArray<float>();
	}

	FState()
	{
		EnemyCanSee = TArray<bool>();
		EnemyCanHear = TArray<bool>();
		EnemyPosition = TArray<FVector>();
		ResultVector = TArray<float>();
	}
	TArray<float> ToFloat()
	{
		ResultVector.Empty();
		for (int i = 0; i < EnemyCanSee.Num(); i++)
		{
			float b = EnemyCanSee[i] ? 1.0f : 0.0f;
			ResultVector.Add(b);

			b = EnemyCanHear[i] ? 1.0f : 0.0f;
			ResultVector.Add(b);

			for (int i = 0; i < 3; i++)
			{
				ResultVector.Add(EnemyPosition[i].Component(i));
			}
		}
		ResultVector.Add(DamageDealt);

		return ResultVector;
	}
	TArray<bool> EnemyCanSee;
	TArray<bool> EnemyCanHear;
	TArray<FVector> EnemyPosition;

	float DamageDealt;

	TArray<float> ResultVector;
};

enum Output
{
	MoveTowardsPlayer,
	ShootPlayer,
	Crouch,
	Fallback
};


UCLASS()
class  ANeuralNetwork : public ACharacter
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	ANeuralNetwork();

	UFUNCTION(BlueprintCallable, Category = Enemies)
		void StartNeuralNetwork(TArray<int> topology);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	virtual void Tick(float delta) override;

	void GatherInputs(FState& empty);

	void FeedForward();

	void BackPropagation();

	void SetErrors();

	float GetTotalError() { return this->error; }

	TArray<float> GetErrors() { return this->errors; }

	void GeneticallyModify();

	void SendOutResult();

	void MoveToPlayer(int index);

	void CrouchAI(int index);

	void ShootAtPlayer(int index);

	void FallbackFromPlayer(int index);

	Matrix* GetNeuronMatrix(int index) { return this->layers[index]->GetMatrixRepresentation(); }
	Matrix* GetActivatedNeuronMatrix(int index) { return this->layers[index]->GetActivatedMatrixRepresentation(); }
	Matrix* GetDerivedNeuronMatrix(int index) { return this->layers[index]->GetDerivedMatrixRepresentation(); }

	void SetNeuronVal(int indexLayer, int indexNeuron, float val) { this->layers[indexLayer]->SetVal(indexNeuron, val); }

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Enemies)
		TArray<ACharacter*> AI;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Player)
		ACharacter* MainPlayer;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Enemies)
		float speed;

	TArray<int> topology;
	TArray<Layer*> layers;
	
	//n-1 size of topology
	TArray<Matrix*> weightMatricies;
	TArray<Matrix*> gradientMatricies;

	TArray<float> target;

	//Error
	float error;
	TArray<float> errors;
	TArray<float> historicalError;

	Output currTick;
}; 
