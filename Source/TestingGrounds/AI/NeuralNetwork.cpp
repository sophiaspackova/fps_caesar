// Fill out your copyright notice in the Description page of Project Settings.

#include "NeuralNetwork.h"
#include <cmath>
#include <stdlib.h>
#include <ctime>
#include <random>
#include "Player/FirstPersonCharacter.h"


// Sets default values for this component's properties
ANeuralNetwork::ANeuralNetwork()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryActorTick.bCanEverTick = true;



}

void ANeuralNetwork::StartNeuralNetwork(TArray<int> topology)
{

	this->topology = topology;

	for (int i = 0; i < topology.Num(); i++)
	{
		Layer* l = new  Layer(topology[i]);
		this->layers.Add(l);
	}

	for (int i = 0; i < topology.Num() - 1; i++)
	{
		Matrix * m = new Matrix(topology[i], topology[i + 1], true);

		this->weightMatricies.Add(m);
	}

	int outputLayerIndex = this->layers.Num() - 1;
	int numOfNeurons = this->layers[outputLayerIndex]->GetNeurons().size();
	for (int i = 0; i < numOfNeurons; i++)
	{
		target.Add(rand() % 2);
		errors.Add(0.0);
	}
}


// Called when the game starts
void ANeuralNetwork::BeginPlay()
{
	Super::BeginPlay();

	// ...

	TArray<int> Topology = TArray<int>();
	Topology.Add(AI.Num());
	Topology.Add(4);
	Topology.Add(4);
	StartNeuralNetwork(Topology);

}

void ANeuralNetwork::Tick(float delta)
{
	FState empty = FState(AI.Num());
	GatherInputs(empty);
	FeedForward();
	GeneticallyModify();
	SetErrors();
	BackPropagation();
	SendOutResult();
}

void ANeuralNetwork::GatherInputs(FState& empty)
{
	for (int i = 0; i < AI.Num(); i++)
	{
		empty.EnemyPosition[i] = AI[i]->GetActorLocation();
		UE_LOG(LogTemp, Display, TEXT("Current Enemy x pos:  %f y pos: %f z pos: %f"), empty.EnemyPosition[i].X, empty.EnemyPosition[i].Y, empty.EnemyPosition[i].Z);
	} 
}

void ANeuralNetwork::GeneticallyModify()
{

	int outputLayerIndex = this->layers.Num() - 1;
	int numOfNeurons = this->layers[outputLayerIndex]->GetNeurons().size();

	int randIndex = rand() % numOfNeurons;

	this->target[randIndex] = this->target[randIndex] / 2.0f;

	if (this->target[randIndex] == 0.0f) this->target[randIndex] = 1.0f;


}

void ANeuralNetwork::SetErrors()
{
	if (this->target.Num() == 0)
	{
		return;
	}
	if (this->target.Num() != this->layers[this->layers.Num() - 1]->GetSize())
	{
		return;
	}

	this->error = 0.0f;

	int outputLayerIndex = this->layers.Num() - 1;
	std::vector<Neuron*> outputNeurons = this->layers[outputLayerIndex]->GetNeurons();
	float highestVal = -10000000000000.0f;

	for(int i = 0; i < target.Num(); i++)
	{ 
		float tempErr = outputNeurons[i]->GetActivatedVal() - target[i];
		if (tempErr > highestVal)
		{
			highestVal = tempErr;
			currTick = (Output) i;
		}
		errors[i] = tempErr;
		this->error += tempErr;
	}

	historicalError.Add(this->error);
}

void ANeuralNetwork::FeedForward()
{
	for (int i = 0; i < this->layers.Num() - 1; i++)
	{
		Matrix* a = this->GetNeuronMatrix(i);

		if (i != 0)
		{
			a = this->GetActivatedNeuronMatrix(i);
		}

		Matrix* b = this->weightMatricies[i];
		Matrix* c = a->Multiply(b);

		if (c != nullptr)
		{
			for (int c_index = 0; c_index < c->GetNumCols(); c_index++)
			{
				this->SetNeuronVal(i + 1, c_index, c->GetVal(0, c_index));
			}
		}
	}
}

void ANeuralNetwork::BackPropagation()
{
	TArray<Matrix* > newWeights = TArray<Matrix*>();
	Matrix* gradients;

	int outputLayerIndex = this->layers.Num() - 1;

	Matrix * derivedYToZ = this->layers[outputLayerIndex]->GetDerivedMatrixRepresentation();

	Matrix* gradientsYToZ = new Matrix(1, this->layers[outputLayerIndex]->GetSize(), false);

	for (int i = 0; i < this->errors.Num(); i++)
	{
		float d = derivedYToZ->GetVal(0, i);
		float e = this->errors[i];
		float g = d * e;
		gradientsYToZ->SetVal(0, i, g);
	}

	int lastHiddenLayerIndex = outputLayerIndex - 1;


	Layer* lastHiddenLayer = this->layers[lastHiddenLayerIndex];

	Matrix* weightsOutputToHidden = this->weightMatricies[lastHiddenLayerIndex];

	Matrix* deltaOutputHidden = (gradientsYToZ->Transpose())->Multiply(lastHiddenLayer->GetActivatedMatrixRepresentation());

	Matrix* newWeightsOutputToHidden = new Matrix(deltaOutputHidden->GetNumRows(), deltaOutputHidden->GetNumCols(), false);

	for (int r = 0; r < deltaOutputHidden->GetNumRows(); r++)
	{
		for (int c = 0; c < deltaOutputHidden->GetNumCols(); c++)
		{
			float originalWeights = weightsOutputToHidden->GetVal(r, c);
			float deltaWeight = deltaOutputHidden->GetVal(r, c);
			float newWeight = originalWeights - deltaWeight;
			newWeightsOutputToHidden->SetVal(r, c, newWeight);
		}
	}

	newWeights.Add(newWeightsOutputToHidden);
	gradients = new Matrix(gradientsYToZ->GetNumRows(), gradientsYToZ->GetNumCols(), false);
	for (int r = 0; r < gradientsYToZ->GetNumRows(); r++)
	{
		for (int c = 0; c < gradientsYToZ->GetNumCols(); c++)
		{
			gradients->SetVal(r, c, gradientsYToZ->GetVal(r, c));
		}
	}

	for (int i = lastHiddenLayerIndex; i > 0; i--)
	{
		Layer* l = this->layers[i];
		Matrix* derivedHidden = l->GetDerivedMatrixRepresentation();
		Matrix* activatedHidden = l->GetActivatedMatrixRepresentation();

		Matrix* derivedGradients = new Matrix(
			1,
			l->GetSize(),
			false
		);

		Matrix* weightMatrix = this->weightMatricies[i];
		Matrix* originalMatrix = this->weightMatricies[i - 1];
		for (int r = 0; r < weightMatrix->GetNumRows(); r++)
		{
			float sum = 0.0f;
			for (int c = 0; c < weightMatrix->GetNumCols(); c++)
			{
				float p = gradients->GetVal(0, c) * weightMatrix->GetVal(r, c);
				sum += p;
			}
			float g = sum * activatedHidden->GetVal(0, r);
			derivedGradients->SetVal(0, r, g);

		}
		Matrix*  leftNeurons = (i-1) == 0 ? this->layers[i]->GetMatrixRepresentation() : this->layers[i - 1]->GetActivatedMatrixRepresentation();
		
		Matrix* deltaWeights = (derivedGradients->Transpose())->Multiply(leftNeurons)->Transpose();


		Matrix* newWeightsHidden = new Matrix(
							deltaWeights->GetNumRows(),
							deltaWeights->GetNumCols(),
							false
							);

		for (int r = 0; r < newWeightsHidden->GetNumRows(); r++)
		{
			for (int c = 0; c <  newWeightsHidden->GetNumCols(); c++)
			{
				float w = originalMatrix->GetVal(r, c);
				float d = deltaWeights->GetVal(r, c);

				float n = w - d;
				newWeightsHidden->SetVal(r, c, n);
			}
		}

		gradients = new Matrix(derivedGradients->GetNumRows(), derivedGradients->GetNumCols(), false);
		for (int r = 0; r < derivedGradients->GetNumRows(); r++)
		{
			for (int c = 0; c < derivedGradients->GetNumCols(); c++)
			{
				gradients->SetVal(r, c, derivedGradients->GetVal(r, c));
			}
		}

		newWeights.Add(newWeightsHidden);
	}

	weightMatricies.Empty();

	for (int index = newWeights.Num() - 1; index >= 0; index--)
	{
		this->weightMatricies.Add(newWeights[index]);
	}
}

void ANeuralNetwork::MoveToPlayer(int index)
{
	if (MainPlayer != nullptr)
	{
		FVector to = MainPlayer->GetActorLocation() - AI[index]->GetActorLocation();

		FRotator s = to.ToOrientationRotator();
		AI[index]->SetActorRotation(s);

		to = AI[index]->GetActorLocation() + to * speed;
		AI[index]->SetActorLocation(to);
	}
}

void ANeuralNetwork::CrouchAI(int index)
{
	if (MainPlayer != nullptr)
	{
		
	}
}

void ANeuralNetwork::ShootAtPlayer(int index)
{
	if (MainPlayer != nullptr)
	{

	}
}

void ANeuralNetwork::FallbackFromPlayer(int index)
{
	FVector bkwd = AI[index]->GetActorForwardVector() * -1;
	bkwd = AI[index]->GetActorLocation() + bkwd * speed;
	AI[index]->SetActorLocation(bkwd);
}


void ANeuralNetwork::SendOutResult()
{
	for (int i = 0; i < AI.Num(); i++)
	{
		switch (currTick)
		{
		case MoveTowardsPlayer: MoveToPlayer(i);
		//case Crouch: CrouchAI(i);
		case ShootPlayer: ShootAtPlayer(i);
		case Fallback: FallbackFromPlayer(i);
		}
	}
}