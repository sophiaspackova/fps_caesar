﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "EnemyAIController.h"
#include "AI/Navigation/NavigationPath.h"
#include "Runtime/AIModule/Classes/AITypes.h"
#include "Runtime/AIModule/Classes/AIController.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavigationSystem.h"
#include "EngineUtils.h"
#include "Runtime/Engine/Classes/Engine/StaticMeshActor.h"
#include "Weapons/BallProjectile.h"
#include "Weapons/Gun.h"


AActor * currentTarget;
AActor * targets[4];
AActor *closestCover;
FVector currentDestination;
float MovementProximity = 30.0f;
float TweakRange = 300.0f;	//The radius for position tweak calculation
float normalWallHeight = 2.4f;
float parapetHeight = 0.6f;
float lineOfSight = 1000.0f;

//Temporary waypoints for scriptedAI
FVector obj1 = FVector(-3550.0f, -360.0f, 226.0f);
FVector obj2 = FVector(-690.0f, 1410.0f, 226.0f);
FVector obj3 = FVector(1510.0f, 300.0f, 226.0f);
FVector obj4 = FVector(2990.0f, 260.0f, 226.0);
FVector obj5 = FVector(3400.0f, 2450.0f, 226.0f);
bool onRouteToObj1 = false;
bool onRouteToObj2 = false;
bool onRouteToObj3 = false;
bool onRouteToObj4 = false;
bool onRouteToObj5 = false;
bool reachObj1 = false;


bool reachObj2 = false;
bool reachObj3 = false;
bool reachObj4 = false;
bool reachObj5 = false;
bool canExecuteOnce = true;

bool inCombat = false;

//int damageReceived;

void AEnemyAIController::BeginPlay()
{
	Super::BeginPlay();

	canExecuteOnce = true;
	inCombat = false;
	GetTargets("PatrollingGuard");
	//StartMove();

	damageReceived = 0;
}

void AEnemyAIController::Tick(float DeltaTime)
{

	Super::Tick(DeltaTime);
	if (targets[0] && targets[1] && targets[2] && targets[3])
	{
		if (DetectEnemyInSight())
		{
			inCombat = true;
		}
		else
		{
			inCombat = false;
		}
	}

	if (!inCombat)
	{
		if (FVector::Dist(GetPawn()->GetActorLocation(), obj1) <= MovementProximity * 6 && onRouteToObj1)
		{
			reachObj1 = true;
		}
		if (FVector::Dist(GetPawn()->GetActorLocation(), obj2) <= MovementProximity * 6 && onRouteToObj2)
		{
			reachObj2 = true;
		}
		if (FVector::Dist(GetPawn()->GetActorLocation(), obj3) <= MovementProximity * 6 && onRouteToObj3)
		{
			reachObj3 = true;
		}
		if (FVector::Dist(GetPawn()->GetActorLocation(), obj4) <= MovementProximity * 6 && onRouteToObj4)
		{
			reachObj4 = true;
		}
		if (FVector::Dist(GetPawn()->GetActorLocation(), obj5) <= MovementProximity * 6 && onRouteToObj5)
		{
			reachObj5 = true;
		}

		if (reachObj1 && onRouteToObj1) {
			onRouteToObj1 = false;
			canExecuteOnce = true;
			ExecuteOnce("obj2");
		}
		if (reachObj2 && onRouteToObj2) {
			onRouteToObj2 = false;
			canExecuteOnce = true;
			ExecuteOnce("obj3");
		}
		if (reachObj3 && onRouteToObj3) {
			onRouteToObj3 = false;
			canExecuteOnce = true;
			ExecuteOnce("obj4");

		}
		if (reachObj4 && onRouteToObj4) {

			onRouteToObj4 = false;
			canExecuteOnce = true;
			ExecuteOnce("obj5");
		}
		if (reachObj5 && onRouteToObj5)
		{
			onRouteToObj5 = false;
			UE_LOG(LogTemp, Warning, TEXT("Round is end"));
		}
	}
	else
	{
		AAIController* ai = Cast<AAIController>(this);
		ai->StopMovement();
		//Shoot();
	}


	//below for input control
	/*
	if (IsInputKeyDown(EKeys::S))
	{

	}
	*/
}

float AEnemyAIController::DetectEnemyInSight()
{
	float result = false;
	if (FVector::Dist(GetPawn()->GetActorLocation(), targets[0]->GetActorLocation()) <= lineOfSight)
	{
		result = result || true;
		currentTarget = targets[0];
	}
	if (FVector::Dist(GetPawn()->GetActorLocation(), targets[1]->GetActorLocation()) <= lineOfSight)
	{
		result = result || true;
		currentTarget = targets[1];
	}
	if (FVector::Dist(GetPawn()->GetActorLocation(), targets[2]->GetActorLocation()) <= lineOfSight)
	{
		result = result || true;
		currentTarget = targets[2];
	}
	if (FVector::Dist(GetPawn()->GetActorLocation(), targets[3]->GetActorLocation()) <= lineOfSight)
	{
		result = result || true;
		currentTarget = targets[3];
	}

	return result;

}

void AEnemyAIController::ExecuteOnce(FString order)
{
	if (canExecuteOnce)
	{
		if (order.Equals("obj1"))
		{
			onRouteToObj1 = true;
			MoveToPosition(obj1, MovementProximity);
		}
		if (order.Equals("obj2"))
		{
			onRouteToObj2 = true;
			MoveToPosition(obj2, MovementProximity);
		}
		if (order.Equals("obj2"))
		{
			onRouteToObj2 = true;
			MoveToPosition(obj2, MovementProximity);
		}
		if (order.Equals("obj3"))
		{
			onRouteToObj3 = true;
			MoveToPosition(obj3, MovementProximity);
		}
		if (order.Equals("obj4"))
		{
			onRouteToObj4 = true;
			MoveToPosition(obj4, MovementProximity);
		}
		if (order.Equals("obj5"))
		{
			onRouteToObj5 = true;
			MoveToPosition(obj5, MovementProximity);
		}
		canExecuteOnce = false;
	}
}


void AEnemyAIController::StartMove()
{
	ExecuteOnce("obj1");
}

void AEnemyAIController::SetTarget(FString name)
{
	TActorIterator< AStaticMeshActor > ActorItr =
		TActorIterator< AStaticMeshActor >(GetWorld());

	while (!ActorItr->GetName().Equals(*name))
	{

		++ActorItr;
	}

	currentTarget = *ActorItr;

	UE_LOG(LogTemp, Warning, TEXT("Set target by input: %s"), *currentTarget->GetName());
}


float AEnemyAIController::MoveToPosition(FVector destination, float proximity)
{
	UNavigationPath* path = UNavigationSystem::FindPathToLocationSynchronously(Cast<UObject>(GetWorld()), GetPawn()->GetActorLocation(), destination);

	//UE_LOG(LogTemp, Warning, TEXT("l: %f"), path->GetPathLength());

	if (path && path->IsValid())
	{

		//UE_LOG(LogTemp, Warning, TEXT("Path Created"));
		FAIMoveRequest req;

		req.SetAcceptanceRadius(proximity);
		req.SetUsePathfinding(true);

		AAIController* ai = Cast<AAIController>(this);

		if (ai)
		{
			ai->RequestMove(req, path->GetPath());
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Couldnt find a path, the destination might not be covered by navmesh"));		//Left for further changes ¯\_(ツ)_/¯
		return false;
	}
	return true;
}


float AEnemyAIController::MoveToPosition(FVector destination, float proximity, FString followingAction)
{
//under construction
	return false;
}


void AEnemyAIController::MoveTest()
{
	UE_LOG(LogTemp, Warning, TEXT("Start MoveTest."));

	FVector pos = FVector(-1010.0f, 990.0f, 226.0f);	//Move to the center of the map
	MoveToPosition(pos, MovementProximity);
}

void AEnemyAIController::MoveForward()
{
	FVector direction = GetPawn()->GetActorForwardVector();
	float distance = FMath::RandRange(100, 800);
	FVector newPos = GetPawn()->GetActorLocation() + direction * distance;
	MoveToPosition(newPos, MovementProximity);
}

void AEnemyAIController::MoveForwardTowardsTarget()
{
	UE_LOG(LogTemp, Warning, TEXT("Start MoveForward"));

	FVector pointer = currentTarget->GetActorLocation() - GetPawn()->GetActorLocation();
	FVector newPos;

	if (FVector::Dist(currentTarget->GetActorLocation(), GetPawn()->GetActorLocation()) <= TweakRange * 2)
	{
		newPos = GetPawn()->GetActorLocation() + pointer * 3 / 5;

	}
	else
	{
		newPos = GetPawn()->GetActorLocation() + pointer / 2;	//left for further changes

		float xTweak = FMath::FRandRange(-TweakRange, TweakRange);
		float yTweak = FMath::FRandRange(-TweakRange, TweakRange);
		newPos = FVector(newPos.X + xTweak, newPos.Y + yTweak, newPos.Z);

	}

	MoveToPosition(newPos, MovementProximity);
}

void AEnemyAIController::MoveBackwardsTowardsTarget()
{
	UE_LOG(LogTemp, Warning, TEXT("Start MoveBackwards"));
	FVector pointer = currentTarget->GetActorLocation() - GetPawn()->GetActorLocation();
	FVector newPos = GetPawn()->GetActorLocation() - pointer / 2;	//left for further changes

	int xTweak = FMath::RandRange(-TweakRange, TweakRange);
	int yTweak = FMath::RandRange(-TweakRange, TweakRange);
	newPos = FVector(newPos.X + xTweak, newPos.Y + yTweak, newPos.Z);

	MoveToPosition(newPos, MovementProximity);
}

void AEnemyAIController::Flank()
{
	UE_LOG(LogTemp, Warning, TEXT("Start Flank"));
	int rotDegree = 45;	//rotation for flanking route
	int side = FMath::RandRange(0, 1);
	FRotator rot(0, rotDegree, 0);
	FVector pointer = currentTarget->GetActorLocation() - GetPawn()->GetActorLocation();
	FVector newPos;
	float xTweak;
	float yTweak;

	if (side == 1)
	{
		//UE_LOG(LogTemp, Warning, TEXT("flanking from the right"));
	}
	else
	{
		//UE_LOG(LogTemp, Warning, TEXT("flanking from the left"));
		rot.Yaw = -rot.Yaw;

	}
	if (FVector::Dist(currentTarget->GetActorLocation(), GetPawn()->GetActorLocation()) <= TweakRange * 2)
	{
		UE_LOG(LogTemp, Warning, TEXT("Too close, no need for flanking"));	//Might need to be tweaked
		return;
	}
	else
	{
		pointer = pointer * 2 / 3;
		newPos = GetPawn()->GetActorLocation() + rot.RotateVector(pointer);	//move a little bit further since AI change its' moving direction
	}

	float canReach = MoveToPosition(newPos, MovementProximity);
	while (!canReach)
	{
		pointer = pointer * 2 / 3;
		//UE_LOG(LogTemp, Warning, TEXT("pointer: %f %f %f"), pointer.X, pointer.Y, pointer.Z);
		newPos = GetPawn()->GetActorLocation() + rot.RotateVector(pointer);
		//UE_LOG(LogTemp, Warning, TEXT("newPos: %f %f %f"), newPos.X, newPos.Y, newPos.Z);
		canReach = MoveToPosition(newPos, MovementProximity);
	}
	xTweak = FMath::RandRange(-TweakRange / 2, TweakRange / 2);
	yTweak = FMath::RandRange(-TweakRange / 2, TweakRange / 2);
	newPos = FVector(newPos.X + xTweak, newPos.Y + yTweak, newPos.Z);
	//UE_LOG(LogTemp, Warning, TEXT("count: %d"), count);
}

void AEnemyAIController::MoveToClosestCover()
{
	UE_LOG(LogTemp, Warning, TEXT("Start MoveToClosestCover"));
	TActorIterator< AStaticMeshActor > WallItr = TActorIterator< AStaticMeshActor >(GetWorld());
	float minDistance = 99999.0f;	//Magic number because I'm lazy

	int count = 0;
	while (WallItr)
	{
		if (WallItr->GetName().Contains("Wall"))
		{
			if (FVector::Dist(WallItr->GetActorLocation(), GetPawn()->GetActorLocation()) < minDistance)
			{
				minDistance = FVector::Dist(WallItr->GetActorLocation(), GetPawn()->GetActorLocation());
				closestCover = *WallItr;
			}
			//UE_LOG(LogTemp, Warning, TEXT("find: %s"), *WallItr->GetName());
			++count;
		}
		++WallItr;
	}

	//(LogTemp, Warning, TEXT("count: %d"), count);
	if (closestCover)
	{
		FVector pointer = currentTarget->GetActorLocation() - closestCover->GetActorLocation();
		FVector unitVector = pointer / (FVector::Dist(currentTarget->GetActorLocation(), closestCover->GetActorLocation()));

		FVector newPos = closestCover->GetActorLocation() - unitVector * 50;

		if (closestCover->GetActorScale3D().Z == normalWallHeight)

		{
			MoveToPosition(newPos, 0);
		}
		else if (closestCover->GetActorScale3D().Z == parapetHeight)
		{
			MoveToPosition(newPos, 0);	//under construction: need to attach following action
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Could not find closest cover, something went wrong!"));
	}
}

//under construction
void AEnemyAIController::Shoot(ACharacter* target /* = nullptr*/)
{
	UE_LOG(LogTemp, Warning, TEXT("Start Shoot"));
	if (target == nullptr)
	{
		AActor * bullet;

		TActorIterator< AStaticMeshActor > ActorItr = TActorIterator< AStaticMeshActor >(GetWorld());

		while (!ActorItr->GetName().Equals("Target"))
		{

			++ActorItr;
		}

		bullet = *ActorItr;
	}
	FActorSpawnParameters SpawnInfo;
	//GetWorld()->SpawnActor<>(Location, Rotation, SpawnInfo);
	FActorSpawnParameters ActorSpawnParams;
	ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

	// spawn the projectile at the muzzle
	GetWorld()->SpawnActor<ABallProjectile>(ProjectileClass, GetPawn()->GetActorLocation(), GetPawn()->GetActorRotation(), ActorSpawnParams);
}

//under construction
void AEnemyAIController::SetAnimation(FString state)
{
	UE_LOG(LogTemp, Warning, TEXT("Start SetAnimation: %s"), *state);

}

void AEnemyAIController::GetTargets(FString name)
{
	TActorIterator< AActor > ActorItr = TActorIterator< AActor >(GetWorld());
	int count = 0;
	while (ActorItr)
	{

		if (ActorItr->GetName().Contains(*name))
		{
			targets[count] = *ActorItr;
			count++;
		}
		++ActorItr;
	}

}

//Haven't tested X_X
void AEnemyAIController::Peek()
{
	if (currentTarget)
	{
		FVector originalPos = GetPawn()->GetActorLocation();
		FVector pointer = currentTarget->GetActorLocation() - GetPawn()->GetActorLocation();
		FVector unitVector = pointer / (FVector::Dist(currentTarget->GetActorLocation(), GetPawn()->GetActorLocation()));
		FVector peekRight = FVector(unitVector.Y, -unitVector.X, unitVector.Z);
		FVector peekLeft = FVector(-unitVector.Y, unitVector.X, unitVector.Z);

		int side = FMath::RandRange(0, 1);
		if (side == 1)
		{
			FVector newPos = originalPos + peekLeft * 300;
			MoveToPosition(newPos, 0);
		}
		else
		{
			FVector newPos = originalPos + peekRight * 300;
			MoveToPosition(newPos, 0);
		}
	}
}

void AEnemyAIController::SwitchTarget()
{
	//under construction
}

FVector AEnemyAIController::GetLocation()
{
	return GetPawn()->GetActorLocation();
}

FRotator AEnemyAIController::GetRotation()
{
	return GetPawn()->GetActorRotation();
}


