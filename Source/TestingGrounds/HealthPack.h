// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AI/PatrollingGuard.h"
#include "HealthPack.generated.h"

UCLASS()
class TESTINGGROUNDS_API ABuffPack : public ACharacter
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABuffPack();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Details")
		int Price;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Details")
		float Buff;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Details")
		float BuffLength;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Details")
		UTexture2D* BuffIcon;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Details")
		FString BuffName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Details")
		int BuffNum;

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void ModifyPlayerStat(APatrollingGuard* player);
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		bool GetCanSee();
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void Buy();
	
};
