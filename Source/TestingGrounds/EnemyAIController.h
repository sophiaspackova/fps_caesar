// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "EnemyAIController.generated.h"

/**
 * 
 */
UCLASS()
class TESTINGGROUNDS_API AEnemyAIController : public AAIController
{
	GENERATED_BODY()
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;



public:
	void Tick(float DeltaTime);

	//Unittest for move functionality
	void MoveTest();

	//Make sure the scriptedAI is moving to next watpoint
	void ExecuteOnce(FString order);

	//choose one action to execute
	void ChooseAction();

	void MoveForward();

	//Move forward towards target
	void MoveForwardTowardsTarget();

	//Move backwards towards target
	void MoveBackwardsTowardsTarget();

	//Set target by name
	void SetTarget(FString name);

	//Move to destination by setting position
	float MoveToPosition(FVector destination, float proximity);

	float MoveToPosition(FVector destination, float proximity, FString followingAction);

	//Begin the patrol
	void StartMove();

	//Flank to either left or right
	void Flank();

	//Find the closest cover then move to(TBD: behind) it.
	void MoveToClosestCover();

	void Peek();


	float DetectEnemyInSight();

	void SetAnimation(FString state);

	//Retrive target actors by name
	void GetTargets(FString name);

	void Shoot(ACharacter* target = nullptr);

	UPROPERTY(EditAnywhere, Category = Projectile)
		TSubclassOf<class ABallProjectile> ProjectileClass;

	void SwitchTarget();

	FVector GetLocation();

	FRotator GetRotation();

	int damageReceived;
};
