// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"

#include "TrainingAIController.generated.h"


/**
 * 
 */
UCLASS()
class TESTINGGROUNDS_API ATrainingAIController : public AAIController
{
	GENERATED_BODY()
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	ATrainingAIController(const FObjectInitializer& objectInitializer);
	void Tick(float DeltaTime);
	float MoveToPosition(FVector destination, float proximity);
	void MoveToDestination();
	void Tester();
	void LockDetector();
	void CombatLockDetector();
	float DeadZoneCheck(FVector pos);
	void TakeAction();
	void ReadRecord();
	void WriteaRecord(int actionType);
	void WriteRecordWithDamage(FString str);
	void WriteRecordsToFile();
	void SetCurrentTarget();
	void ResetActionAvailability();
	void TrainingShoot();
	void TrainingShootPeek();
	void TrainingMoveToClosestCover();
	void TrainingFlank();
	void TrainingMoveForwardTowardsTarget();
	void TrainingMoveBackwardsTowardsTarget();
	void TrainingChase();
	void UpdateActionDestination(FVector des);
	void WaitToResetActionAvailability();
	void MoveToSecondaryCover();
	void GetTeammatesStats();
	void InflictDamage();
	void TrainingPeek();
	void TrainingPeekBackToOldPos();

	void TrainingWalkAway();

	UFUNCTION()
		void OnCollision(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void OnActorCollision(AActor* OverlappedActor, AActor* OtherActor);
	UFUNCTION()
		void OnComponentHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);


	void PlayVoice(int index);

	void PlayDialoguePreBattle();
	void PlayDialogueSpotEnemy();
	void PlayDialogueLoading();
	void PlayDialogueTransmission();
	void PlayDialogueMoving();
	void PlayDialogueMandown();
	void PlayDialogueShoot();

	//feature values
	bool trainingInCombat = false;
	bool trainingUnderCover = false;
	//bool trainingHasTarget = false;
	bool trainingUnderAttack = false;
	bool distanceToCurrentTarget = true;	//0 for close, 1 for far 

	bool GetIncombatState();
	bool GetUnderCoverState();
	bool GetUnderAttackState();
	bool GetDistanceToCurrentTargetState();

	void SetUnderCoverState();
	void ResetUnderCoverState();
	void ReceiveDamage();
	void ResetUnderAttackState();
	FString newRecords = "";
	

	AActor * trainingCurrentTarget;

	AActor *trainingClosestCover;
	AActor *trainginSecondaryCover;


	void MoveToCoverLockDetector();

	int health;
	int GetHealth();

	int manDownNum = 0;
	int totalDamageDealt = 0;
	int totalDamageDealtPeriod = 0;
	void ResetTotalDamageDealtPeriod();
	int GetDeathState();

	int SortRecordsByState(TArray<FString> arr, FString record);
	int GetOptimalAction(TArray<FString> arr, FString record);


	USoundWave* prebattle1Wave;
	USoundBase* prebattle1Base;
	USoundWave* prebattle2Wave;
	USoundBase* prebattle2Base;
	USoundWave* mandown1Wave;
	USoundBase* mandown1Base;
	USoundWave* mandown2Wave;
	USoundBase* mandown2Base;
	USoundWave* mandown3Wave;
	USoundBase* mandown3Base;
	USoundWave* mandown4Wave;
	USoundBase* mandown4Base;
	USoundWave* moving1Wave;
	USoundBase* moving1Base;
	USoundWave* moving2Wave;
	USoundBase* moving2Base;
	USoundWave* moving3Wave;
	USoundBase* moving3Base;
	USoundWave* moving4Wave;
	USoundBase* moving4Base;
	USoundWave* moving5Wave;
	USoundBase* moving5Base;
	USoundWave* reloading1Wave;
	USoundBase* reloading1Base;
	USoundWave* reloading2Wave;
	USoundBase* reloading2Base;
	USoundWave* reloading3Wave;
	USoundBase* reloading3Base;
	USoundWave* reloading4Wave;
	USoundBase* reloading4Base;
	USoundWave* rifleburst1Wave;
	USoundBase* rifleburst1Base;
	USoundWave* rifleburst2Wave;
	USoundBase* rifleburst2Base;
	USoundWave* shoot1Wave;
	USoundBase* shoot1Base;
	USoundWave* shoot2Wave;
	USoundBase* shoot2Base;
	USoundWave* shoot3Wave;
	USoundBase* shoot3Base;
	USoundWave* shoot4Wave;
	USoundBase* shoot4Base;
	USoundWave* shoot5Wave;
	USoundBase* shoot5Base;
	USoundWave* shoot6Wave;
	USoundBase* shoot6Base;
	USoundWave* spotenemy1Wave;
	USoundBase* spotenemy1Base;
	USoundWave* spotenemy2Wave;
	USoundBase* spotenemy2Base;
	USoundWave* spotenemy3Wave;
	USoundBase* spotenemy3Base;
	USoundWave* spotenemy4Wave;
	USoundBase* spotenemy4Base;
	USoundWave* spotenemy5Wave;
	USoundBase* spotenemy5Base;
	USoundWave* spotenemy6Wave;
	USoundBase* spotenemy6Base;
	USoundWave* spotenemy7Wave;
	USoundBase* spotenemy7Base;
	USoundWave* transmission1Wave;
	USoundBase* transmission1Base;
	USoundWave* transmission2Wave;
	USoundBase* transmission2Base;
	USoundWave* transmission3Wave;
	USoundBase* transmission3Base;
	USoundWave* transmission4Wave;
	USoundBase* transmission4Base;
	USoundWave* transmission5Wave;
	USoundBase* transmission5Base;
	USoundWave* transmission6Wave;
	USoundBase* transmission6Base;
	USoundWave* transmission7Wave;
	USoundBase* transmission7Base;

private:
	bool canTakeAction;
	bool canDialogueSpotEnemy;
	FVector actionDestination;
	AActor* otherTrainee1;
	AActor* otherTrainee2;

	AActor* otherTrainee3;
	FVector peekOldPos;
	FVector previousPos;
	FVector combatPreviousPos;
	bool canStartCombatLockDetect;
	int receivedDamage = 10;
	int distanceJudger = 800;
	float canDelay;
	FVector graveyard = FVector(-450, -2010, 236);

	int lastActionTook = 0;
	int chaseDistance = 1600;
	int walkAwayDistance = 400;
	bool canDie = true;

	TArray<FString> records = {};

	
	int trainingType = 0;
	FString projectDir;
	bool isDead = false;
	FString currentRecord = "";

	TArray<FString> newRecordsWithoutDamage = {};

	bool isPeeking = false;

	TArray < FString > FourMenAlive = {};
	TArray < FString > ThreeMenAlive = {};
	TArray < FString > TwoMenAlive = {};
	TArray < FString > OneMenAlive = {};
};