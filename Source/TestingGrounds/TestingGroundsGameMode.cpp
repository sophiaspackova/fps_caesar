// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "TestingGroundsGameMode.h"
#include "TestingGroundsHUD.h"
#include "Player/FirstPersonCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include <fstream>
#include <string>

ATestingGroundsGameMode::ATestingGroundsGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/Player/Behaviour/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ATestingGroundsHUD::StaticClass();

	OverallScore = 0;
	CurrentScore = 0;
	NumberOfKills = 0;
	TotalTimeAlive = 0;
}

void ATestingGroundsGameMode::ResetLevel()
{
	OverallScore = 0;
	CurrentScore = 0;
	NumberOfKills = 0;
	TotalTimeAlive = 0;

	ResetActors();
}

int ATestingGroundsGameMode::UpdateCurrScore(int addedScore)
{
	OverallScore += addedScore;
	CurrentScore += addedScore;

	return CurrentScore;
}

int ATestingGroundsGameMode::UpdateKills(int kills)
{
	NumberOfKills += kills;
	return NumberOfKills;
}

void ATestingGroundsGameMode::UpdateAliveTime(float deltaTime)
{
	TotalTimeAlive += deltaTime;
}

float ATestingGroundsGameMode::GetTimeAlive()
{
	return TotalTimeAlive;
}

void ATestingGroundsGameMode::EndGame()
{
	std::ifstream file("save.txt");
	std::string line;

	TArray<FPlayerScore> list = TArray<FPlayerScore>();

	int MaxScoreIndex, MaxScore = 0;
	//int SecondaryScoreIndex, SecondaryScore = 0;
	//int TrinaryScoreIndex, TrinaryScore = 0;

	if(file)
	{ 
		while (getline(file, line))
		{
			FPlayerScore currPlayer;
			//there is a user left

			//Time alive
			getline(file, line);
			currPlayer.TimeAlive = atof(line.c_str());

			//number of kills
			getline(file, line);
			currPlayer.NumKills = atoi(line.c_str());

			//score number
			getline(file, line);
			currPlayer.Score = atoi(line.c_str());
			if (currPlayer.Score > MaxScore)
			{
				MaxScore = currPlayer.Score;
				MaxScoreIndex = list.Num();
			}

			list.Add(currPlayer);
		}
		file.close();
	}
}

void ATestingGroundsGameMode::GetHighScores()
{

}

void ATestingGroundsGameMode::NewScore(int newScore, FString username, float totalTimeAlive, int numKills)
{

}