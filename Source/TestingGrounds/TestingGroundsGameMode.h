// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TestingGroundsHUD.h"
#include "TestingGroundsGameMode.generated.h"


USTRUCT(BlueprintType)
struct FPlayerScore
{
	GENERATED_BODY()
	float TimeAlive;
	int NumKills;
	int Score;
};


UCLASS(minimalapi)
class ATestingGroundsGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATestingGroundsGameMode();

	UFUNCTION(BlueprintCallable)
		void ResetLevel();

	UFUNCTION(BlueprintImplementableEvent)
		void ResetActors();

	UFUNCTION(BlueprintCallable)
		int UpdateCurrScore(int addedScore);

	UFUNCTION(BlueprintCallable)
		int UpdateKills(int kills);

	UFUNCTION(BlueprintCallable)
		void UpdateAliveTime(float deltaTime);

	UFUNCTION(BlueprintCallable)
		float GetTimeAlive();

	UFUNCTION(BlueprintCallable)
		void EndGame();

	UFUNCTION(BlueprintCallable)
		void GetHighScores();

	UFUNCTION(BlueprintCallable)
		void NewScore(int newScore, FString username, float totalTimeAlive, int numKills);

protected:
	UPROPERTY(BlueprintReadWrite)
		int OverallScore;
	UPROPERTY(BlueprintReadWrite)
		int CurrentScore;
	UPROPERTY(BlueprintReadWrite)
		int NumberOfKills;
	UPROPERTY(BlueprintReadWrite)
		float TotalTimeAlive;

	UPROPERTY(BlueprintReadWrite)
	ATestingGroundsHUD* MainGameHUD;
	UPROPERTY(BlueprintReadWrite)
	ATestingGroundsHUD* EndGameHUD;

};



