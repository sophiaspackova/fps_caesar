# README #
>This repo is for CS 524 Tactical AI (Project Caesar)

# Setup #
>Please read documentation for [SourceTree](https://www.sourcetreeapp.com/). SourceTree is a simple appliaction for source control.
>You might also want to learn git commands

# Contribution guidelines #
- Feel free to edit README.
- [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
- Make sure you do not commit unfinished work to **master**. Creating your own branch is **recommended**.


# Who do I talk to? #
- Guancheng - (guanchen@usc.edu)
- Jian - (jianz@usc.edu)
- Rajat - (rajatlokesh23@gmail.com)
- Sophia - (spackova@usc.edu)
- Soumith - (bsvsoumith1994@gmail.com)
